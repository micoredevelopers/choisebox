<?php
return [
	'default'    => [
		'width'  => 500,
		'height' => 500,
	],
	'watermark'  => [
		'path'     => public_path('images/staff/watermark.png'),
		'position' => 'center',
	],
];