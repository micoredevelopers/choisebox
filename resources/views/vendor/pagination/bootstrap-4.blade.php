@if ($paginator->hasPages())
    <nav class="pagination">
        <ul class="pagination_wrap">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled preBtn paginationBtn" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="page-link" aria-hidden="true">
                        <svg width="10" height="20" viewBox="0 0 10 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.86704 19.8607C10.0443 19.675 10.0443 19.3731 9.86704 19.1873L1.09735 10L9.86704 0.812656C10.0443 0.62694 10.0443 0.325004 9.86704 0.139287C9.68977 -0.0464291 9.40156 -0.0464291 9.22429 0.139287L0.13275 9.6638C0.04456 9.75713 -4.31827e-07 9.87905 -4.37156e-07 10.001C-4.42486e-07 10.1229 0.04456 10.2448 0.13275 10.3362L9.22429 19.8607C9.40156 20.0464 9.68973 20.0464 9.86704 19.8607Z" fill="white"/>
                        </svg>
                    </span>
                </li>
            @else
                <li class="page-item preBtn paginationBtn">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">
                        <svg width="10" height="20" viewBox="0 0 10 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.86704 19.8607C10.0443 19.675 10.0443 19.3731 9.86704 19.1873L1.09735 10L9.86704 0.812656C10.0443 0.62694 10.0443 0.325004 9.86704 0.139287C9.68977 -0.0464291 9.40156 -0.0464291 9.22429 0.139287L0.13275 9.6638C0.04456 9.75713 -4.31827e-07 9.87905 -4.37156e-07 10.001C-4.42486e-07 10.1229 0.04456 10.2448 0.13275 10.3362L9.22429 19.8607C9.40156 20.0464 9.68973 20.0464 9.86704 19.8607Z" fill="white"/>
                        </svg>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item paginationItem active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                        @else
                            <li class="page-item paginationItem"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item nextBtn paginationBtn">
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                         <svg width="10" height="20" viewBox="0 0 10 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.132956 19.8607C-0.0443176 19.675 -0.0443176 19.3731 0.132956 19.1873L8.90265 10L0.132956 0.812656C-0.0443185 0.62694 -0.0443185 0.325004 0.132955 0.139287C0.310229 -0.0464291 0.598439 -0.0464291 0.775713 0.139287L9.86725 9.6638C9.95544 9.75713 10 9.87905 10 10.001C10 10.1229 9.95544 10.2448 9.86725 10.3362L0.775714 19.8607C0.59844 20.0464 0.310268 20.0464 0.132956 19.8607Z" fill="white"/>
                        </svg>
                    </a>
                </li>
            @else
                <li class="page-item disabled nextBtn paginationBtn" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="page-link" aria-hidden="true">
                        <svg width="10" height="20" viewBox="0 0 10 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.132956 19.8607C-0.0443176 19.675 -0.0443176 19.3731 0.132956 19.1873L8.90265 10L0.132956 0.812656C-0.0443185 0.62694 -0.0443185 0.325004 0.132955 0.139287C0.310229 -0.0464291 0.598439 -0.0464291 0.775713 0.139287L9.86725 9.6638C9.95544 9.75713 10 9.87905 10 10.001C10 10.1229 9.95544 10.2448 9.86725 10.3362L0.775714 19.8607C0.59844 20.0464 0.310268 20.0464 0.132956 19.8607Z" fill="white"/>
                        </svg>
                    </span>
                </li>
            @endif
        </ul>
    </nav>
@endif
