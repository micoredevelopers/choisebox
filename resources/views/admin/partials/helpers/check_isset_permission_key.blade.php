@if(empty($permissionKey ?? '') && isSuperAdmin())
    <span class="badge badge-danger">$permissionKey is empty!</span>
@endif