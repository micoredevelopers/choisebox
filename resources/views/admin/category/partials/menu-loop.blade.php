<?php /** @var $item \App\Models\Category\Category */ ?>

<ol class="dd-list" {{--style="display: none;"--}}>
	@foreach($categories as $item)

		<li class="dd-item {{ $item }}" data-id="{{ $item->id }}">
			<div class="row mx-0 bordered border-none b-bottom color-light">
				<div class="col-1 px-0">
					<div class="dd-handle cursor-move ">
						@include('admin.partials.sort_handle_small')
					</div>
				</div>
				<div class="col-8">
					<label>
						<input type="text" data-url-update="{{ route($routeKey . '.update', $item->getKey()) }}"
							   value="{{ $item->getCategoryName() }}" name="name"
							   placeholder="Название" class="form-control">
					</label>
				</div>
				<div class="col-2">
					<img src="{{ getPathToImage($item->image) }}" class="img-fluid" width="40" alt="">
				</div>
				<div class="col-1">
					<div class="text-right">
						<div class="dropdown menu_drop">
							<button
								class="btn btn-secondary dropdown-toggle" type="button"
								id="dropdownMenuButton_{{ $item->id }}" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
								<i class="material-icons">menu</i>
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton_{{ $item->id }}">
								@if($canEditCategory)
									<a href="{{ route($routeKey . '.edit',  $item->id)  }}" class="dropdown-item">@lang('form.edit')</a>
								@endif
								@if($canDeleteCategory)
									{!! Form::open( ['method' => 'delete', 'url' => route($routeKey . '.destroy', $item->id), 'class' => 'formDeleteConfirm']) !!}
									<button type="submit" class="dropdown-item">@lang('form.delete')</button>
									{!! Form::close() !!}
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
	@endforeach
</ol>
