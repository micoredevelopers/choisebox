<?php /** @var $edit \App\Models\Category\Category */ ?>
@include('admin.partials.crud.elements.name')

@include('admin.partials.crud.elements.url')

@include('admin.partials.crud.elements.image-upload-group')

{{--@include('admin.partials.crud.elements.url')--}}

<div class="row">
    <div class="col-md-6">
{{--        @include('admin.partials.crud.elements.active')--}}
    </div>
</div>

<select name="product_type" id="product_type" class="form-control selectpicker" data-live-search="true">
    @forelse(\App\Enum\ProductTypeEnum::getEnums() as $enum)
        @php
            $selected = (isset($edit)) ? ($edit->getAttribute('product_type') == $enum->getKey()): false;
        @endphp
        <option value="{{ $enum->getKey() }}" {!! selectedIfTrue($selected) !!}>
            {{ $enum }}
        </option>
    @empty
        <option value="">{{ __('form.select.empty') }}</option>
    @endforelse
</select>
