<?php /** @var $sliderItem \App\Models\Slider\SliderItem */ ?>
@include('admin.sliders.partials.slides-create', ['slider' => $edit])

@isset($list)
    <div data-sortable-container="true" data-table="slider_items">
        @foreach($list as $sliderItem)
            <div class="draggable bordered b-top double-width pt-4 mb-4" data-sort="true"
                 data-id="{{ $sliderItem->id }}" data-deleteable-row="">
                <div class="row">
                    <div class="col-1">
                        @include('admin.partials.sort_handle')
                    </div>
                    <div class="col-10">
                        @include('admin.partials.crud.elements.image-upload-group',
                    ['name' => inputNamesManager($sliderItem)->getNameInputByKey('src'), 'edit' => $sliderItem, 'editable' => false,])
                    </div>
                    @include('admin.partials.delete_ajax_button', ['item' => $sliderItem,])
                </div>

                    <div class="col-md-9">
                        @include('admin.sliders.elements.link')
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endisset