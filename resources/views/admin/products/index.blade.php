<?php

use App\DataContainers\Product\ProductSearchData;
use App\Models\Category\Category;
use App\Models\Product\Product;

/** @var $category Category */ ?>
<?php /** @var $item Product */ ?>
<?php /** @var  $searchContainer ProductSearchData */ ?>
<?php /** @var $categories \App\Models\Category\Category[] */ ?>
@php
    /** @var $permissionKey string */
        $canEdit = Gate::allows('edit_'. $permissionKey );
        $canDelete = Gate::allows('delete_'. $permissionKey );
@endphp
<form action="" method="get">
    <div class="form-group">
        <div class="row">
            <div class="col-8 pt-2">
                <input type="text" class="form-control" name="search" value="{{ $searchContainer->getSearch() }}" autocomplete="off">
            </div>
            <div class="col-2 pt-1">
                <select name="category" id="category_id" class="form-control" autocomplete="off">
                    <option value="">Выберите категорию</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->getKey() }}"
                                {!! selectedIfTrue($category->getKey() === $searchContainer->getCategoryId()) !!}
                        >{{ $category->getNameDisplay() }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2">
                <button class="btn btn-danger" type="submit"><i class="fa fa-search" aria-hidden="true"></i>
                    @lang('form.search')
                </button>
            </div>
        </div>
    </div>
</form>

@includeIf('admin.partials.helpers.check_isset_permission_key')
<div class="table-responsive">
    <table class="table table-shopping">
        <thead>
        <tr>
            <th class="">@lang('form.image.image')</th>
            <th class="th-description">@lang('form.title')</th>
            <th class="th-description">{{ __('modules.category.title_singular') }}</th>
            <th class="th-description"></th>
            <th class="text-right">
                <a href="{{ route($routeKey.'.create') }}" class="btn btn-primary">@lang('form.create')</a>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $item)
            <tr>
                <td>
                        <img src="{{ getPathToImage($item->image) }}" class="img-fluid" width="60" alt="">
                </td>
                <td>
                    <a href="{{ route($routeKey.'.edit', $item->id) }}">{{ $item->name }}</a>
                </td>
                <td>
                    @if ($category = $item->category)
                        <a href="{{ route(routeKey('categories', 'edit'), $category->id) }}">{{ $category->getCategoryName() }}</a>
                    @endif
                </td>
                <td>
                    @include('admin.partials.preview-button', ['link' => route('category.show') . '?search=' . $item->getNameDisplay() ,])
                </td>
                <td class="text-primary text-right">
                    @include('admin.partials.action.index_actions')
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

{{$products->render()}}

@push('js')<script src="{{ asset('libs/nestable2/jquery.nestable.min.js') }}"></script>@endpush
@push('css')<link rel="stylesheet" href="{{ asset('libs/nestable2/jquery.nestable.min.css') }}">@endpush
