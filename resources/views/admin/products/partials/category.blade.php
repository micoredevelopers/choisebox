<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $edit \App\Models\Product\Product */ ?>
@isset($categories)
    {!! errorDisplay('category_id') !!}
    <label for="category_id">
        @if(isset($edit))
            <a href="{{ $edit->category ? route(routeKey('categories', 'edit'), $edit->category->id) : '#' }}">{{ __('modules.category.title_singular') }}</a>
        @else
            {{ __('modules.category.title_singular') }}
        @endif
    </label>
    <select name="category_id" id="category_id" class="form-control selectpicker" data-live-search="true">
        @forelse($categories as $category)
            @php
                $selected = (isset($edit)) ? ($edit->getAttribute('category_id') == $category->getPrimaryValue() ): false;
            @endphp
            <option value="{{ $category->getPrimaryValue() }}" {!! selectedIfTrue($selected) !!}
            >{{ $category->getCategoryName() }}
            </option>
        @empty
            <option value="">{{ __('form.select.empty') }}</option>
        @endforelse
    </select>
@endisset
