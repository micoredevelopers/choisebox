<?php /** @var $cartCalcData \App\DataContainers\Cart\CartCalcData */ ?>
<?php /** @var $cartData \App\DataContainers\Cart\CartData */ ?>
<?php /** @var $order \App\Models\Order */ ?>
@php
    $deliveryEnum = $order->getOrderDeliveryTypeEnum();
@endphp
@extends('mail.layout.layout')

@section('content')
    <table class="table">
        <tr>
            <td>Имя</td>
            <td>{{ $order->getName() }}</td>
        </tr>
        <tr>
            <td>Фамилия</td>
            <td>{{ $order->getSurname() }}</td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>{{ $order->getEmail() }}</td>
        </tr>
        <tr>
            <td>Способ Доставки</td>
            <td>{{ $deliveryEnum->getTitle() }}</td>
        </tr>
        <tr>
            @if($deliveryEnum->isNovaposhta())
                <td>Город: {{ $order->getCity() }}</td>
                <td>Номер отделения: {{ $order->getBranch() }}</td>
            @elseif($deliveryEnum->isDelivery() || $deliveryEnum->isPersonalTransport())
                <td>Адрес:</td>
                <td>{{ $order->getAddress() }}</td>
            @else
                <td>Пункт выдачи:</td>
                <td>{{ $order->getPickupTypeEnum()->getTitle() }}</td>
            @endif
        </tr>
        <tr>
            <td>Способ Оплаты</td>
            <td>{{ $order->getOrderPaymentTypeEnum()->getTitle() }}</td>
        </tr>
        <tr>
            <td>Телефон</td>
            <td>{{ $order->getPhone() }}</td>
        </tr>
        <tr>
            <td>Комментарий к заказу</td>
            <td>{{ $order->getComment() }}</td>
        </tr>
        <tr>
            <td>Общая сумма заказа:</td>
            <td>{{ $cartCalcData->getCartCost() }} {{ getCurrencyIcon() }}</td>
        </tr>
    </table>
    <table class="table">
        @foreach($order->getOrderProducts() as $orderProduct)
            <tr>
                <td>
                    <img style="max-width: 80px; height:auto; " src="{{ getPathToImage(imgPathOriginal($orderProduct->getProduct()->getImage())) }}" alt="">
                </td>
                <td>
                   {{ $orderProduct->getName() }}
                </td>
                <td>{{ $orderProduct->getPrice() }} {{ getCurrencyIcon() }}</td>
                <td>{{ $orderProduct->getQuantity() }} шт.</td>
                <td>{{ $orderProduct->getCost() }} {{ getCurrencyIcon() }}</td>
            </tr>
        @endforeach

    </table>
@stop