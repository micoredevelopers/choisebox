<?php /** @var $feedback \App\DataContainers\Mail\FeedbackReportMailData */ ?>
@extends('mail.layout.layout')

@section('content')
    <table class="table">
        <tr>
            <td>Имя</td>
            <td>{{ $name ?? '' }}</td>
        </tr>
        <tr>
            <td>Телефон</td>
            <td>{{ $phone ?? '' }}</td>
        </tr>
    </table>
@stop