<!DOCTYPE html>
<html lang="en">
<head>
    @include('public.layout.includes.head')
    <script src="https://unpkg.com/imask"></script>
</head>
<body class="index-page">
@yield('header', view('public.layout.includes.header'))

<div class="wrapper">
    @hasSection('content')
        @yield('content')
    @else
        {!! $content ?? '' !!}
    @endif
</div>
@include('public.layout.includes.footer')
@include('public.layout.includes.assets.scripts')
</body>

</html>