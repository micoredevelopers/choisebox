<footer>
    <div class="container">
        <div class="flex_sb">
            <nav class="flex_sb">
                <li><a href="{{ getSetting('social.facebook') }}" target="_blank" rel="nofollow">Facebook</a></li>
                <li><a href="{{ getSetting('social.instagram') }}" target="_blank" rel="nofollow">Instagram</a></li>
                <li><a href="{{ getSetting('social.telegram') }}" target="_blank" rel="nofollow">Telegram</a></li>
                <li><a href="{{ getSetting('social.viber') }}" target="_blank" rel="nofollow">Viber</a></li>
                <li><a href="tel:+{{ extractDigits(getSetting('global.phone-one')) }}">{{ getSetting('global.phone-one') }}</a></li>
                <li><a href="tel:+{{ extractDigits(getSetting('global.phone-two')) }}">{{ getSetting('global.phone-two') }}</a></li>
            </nav>
            <p>{{ request()->getHost() }} © {{ date('Y') }} by <a target="_blank" href="https://micorestudio.com">MantiCore Development</a></p>
        </div>
    </div>
</footer>