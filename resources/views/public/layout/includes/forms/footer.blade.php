<div class="footer-support">
    <div class="container">
        <div class="form_wrap">
            <div class="wrap"><h3>{{getTranslate('main.form.title', 'Остались вопросы?')}}</h3>
                <p>{{getTranslate('main.form.text', 'Мы с удовольствием ответим на них. Оставьте ваши контактыи наш менеджер свяжется с вами в ближайшее время.')}} </p>
                <form class="form-wrapper" action="{{ route('feedback') }}" method="post">
                    @csrf
                    <input class="input-type-one" required="" minlength="4" name="name" placeholder="{{getTranslate('forms.name', 'Имя')}}">
                    <input class="input-type-one imaskjs__input_tel" type="tel" required="" name="phone"
                           placeholder="{{getTranslate('forms.phone', 'Номер телефона')}}">
                    <button class="button_type-one">{{getTranslate('button.send', 'Отправить')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>