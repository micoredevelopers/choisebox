<?php /** @var $cart \App\Models\Cart */ ?>
<?php /** @var $cartCalcData \App\DataContainers\Cart\CartCalcData */ ?>


<div class="modal_bg" id="basket-modal">
    <div class="container">
        <div class="wrap-mod">
            <div class="wrap top">
                <h2>{{getTranslate('main.cart', 'Корзина')}}</h2>
                <button class="button_type-none basket-close"><img alt="закрить" src="{{ asset('/img/close.svg') }}">
                </button>
            </div>
        </div>
        <div class="product-block">
            <div class="product-scroll">
                <div class="products-wrapper" data-productList>
                    @foreach($cartData->getProducts() as $cartProduct)
                        <div class="product-wrap" data-productItem>
                            <div class="product_img-wrap">
                                <img alt="" src="{{getPathToImage($cartProduct->getProduct()->image)}}">
                            </div>
                            <div class="product_title-wrapper">
                                <div class="product_title-wrap">
                                    <p>{{$cartProduct->getProduct()->getProductName()}}</p>
                                    <div class="prod-modal">{{ $cartProduct->getProduct()->getProductName()}}</div>
                                    <p class="product_price">{{$cartProduct->getProduct()->getPrice()}} грн
                                        @if($cartProduct->getProduct()->getProductTypeEnum()->isBox())
                                            <span>{{$cartProduct->getProduct()->getCapacity()}}</span>
                                        @else
                                            <span>{{$cartProduct->getProduct()->getWeight()}} г</span>
                                        @endif
                                    </p>
                                </div>
                                <div class="product_number-wrap" data-form>
                                    <button class="button_type-one"
                                            data-minusProduct="{{ route('cart.change',['id' => $cartProduct->getProduct()->getKey()]) }}">
                                        <img src='/img/minus.svg'>
                                    </button>
                                    <input min="1" class="mx-1 formInput" type="number"
                                           value="{{ $cartProduct->getQuantity() }}"
                                           data-cart-amount-product-id="{{ $cartProduct->getProduct()->getKey() }}"
                                           data-input="{{ route('cart.change',['id' => $cartProduct->getProduct()->getKey()]) }}">

                                    <button class="button_type-one"
                                            data-plusProduct="{{ route('cart.change',['id' => $cartProduct->getProduct()->getKey()]) }}">
                                        <img src='/img/plus.svg'>
                                    </button>
                                </div>
                                <button data-product-id="{{$cartProduct->getProduct()->getKey()}}"
                                        data-deleteUrl="{{ route('cart.remove', ['id' => $cartProduct->getProduct()->getKey()]) }} "
                                        class="button_type-none deleteBtn"><img alt="" src="/img/delete.svg">
                                </button>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="wrap-mod">
                <div class="wrap bottom">
                    <div class="basket_total-title">
                        <p class="title-total">
                            <span class="basket_total-mob-mod">{{getTranslate('main.cart.total', 'Всего')}}:</span>
                            <span class="total-priсe"> {{$cartCalcData->getCartCost()}} грн</span>
                            <span class="total-weight" data-weight="{{ $cartCalcData->getCartWeight() }}"> {{$cartCalcData->getCartWeight()}} г</span>
                        </p>
                        <p class="title-order"
                           data-minWeight="{{ (int)getSetting('order.minimal_amount_weight') }}">{{getTranslate('main.cart.title', 'Минимальный заказ 150 грамм')}}</p>
                    </div>
                    <a class="button_type-one" id="modalOrderBtn" data-orderBtn href="{{ route('cart.index') }}">
                        Перейти к заказу
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>