<?php /** @var $category Category */

use App\Models\Category\Category; ?>
<div class="modal_bg" id="search">
    <div class="container">
        <form class="search" action="{{ ($category ?? false) ? route('category.show', $category->url) : route('category.show') }}" id="searchForm" name="searchForm">
            <input class="input-search" type="search" name="search" value="{{ request('search') }}" placeholder="По названию конфеты">
            <button class="button_type-none">
                <img alt="закрить" src="{{ asset('/img/close.svg') }}">
            </button>
        </form>
    </div>
</div>