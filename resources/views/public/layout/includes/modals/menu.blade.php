<?php /** @var $category Category */

use App\Models\Category\Category; ?>
<div class="modal_bg" id="menu">
    <div class="container">
        <div class="title">
            <h1>{{getTranslate('main.header.choose', 'Выбрать конфеты')}}</h1>
            <button data-closeMenu class="button_type-none button-search-open">
                <img alt="закрить"
                     src="{{ asset('/img/close.svg') }}">
            </button>
        </div>
        <div class="menu_wrap">
            @foreach($categories as $category)
                <a class="manufacturer" href="{{ route('category.show', $category->getSlug()) }}">
                    <img alt="готовые наборы" src="{{ getPathToImage(imgPathOriginal($category->getImage())) }}">
                    <h2 class="manufacturer_title">{{$category->getCategoryName()}}</h2>
                </a>
            @endforeach
{{--            <a class="manufacturer" href="{{ route('boxes.show')}}">--}}
{{--                <img alt="готовые наборы" src="{{ asset('img/boxes_category_image.png') }}">--}}
{{--                <h2 class="manufacturer_title">Упаковки</h2>--}}
{{--            </a>--}}
        </div>
    </div>
</div>