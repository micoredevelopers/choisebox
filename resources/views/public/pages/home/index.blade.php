@extends('public.layout.app')
@section('content')
    <body class="index-page">

    <div class="content-wrapper">
        @include('public.pages.home.includes.welcome')
        @include('public.pages.home.includes.about')
    </div>
    @include('public.layout.includes.modals.search')
    @include('public.layout.includes.modals.menu')
    @include('public.layout.includes.modals.cart')
    @include('public.layout.includes.forms.footer')

    {{--  <main id="main-page">--}}
    {{--  <a href="/cart/index">Корзина</a>--}}
    {{--  <a href="/cart/checkout">Оформление</a>--}}

    {{--  <tbody>--}}
    {{--  @foreach($products as $product)--}}
    {{--    <tr>--}}
    {{--      <td>--}}
    {{--        <div class="col-2">--}}
    {{--          <img src="{{ getPathToImage($product->image) }}" class="img-fluid" width="40" alt="">--}}
    {{--        </div>--}}
    {{--      </td>--}}
    {{--      <td>--}}
    {{--        <p>{{ $product->name }}</p>--}}
    {{--      </td>--}}
    {{--      <td>--}}
    {{--        <p>{{ $product->weight }}</p>--}}
    {{--      </td>--}}
    {{--      <td>--}}
    {{--        <p>{{ $product->price }}</p>--}}
    {{--      </td>--}}
    {{--      <td>--}}
    {{--        <form action="{{ route('cart.add', ['id' => $product->id]) }}"--}}
    {{--              method="post" class="form-inline">--}}
    {{--          @csrf--}}
    {{--          <label for="input-quantity">Количество</label>--}}
    {{--          <input type="text" name="quantity" id="input-quantity" value="1"--}}
    {{--                 class="form-control mx-2 w-25">--}}
    {{--          <button type="submit" class="btn btn-success">Добавить в корзину</button>--}}
    {{--        </form>--}}
    {{--      </td>--}}
    {{--    </tr>--}}
    {{--  @endforeach--}}
    {{--  </tbody>--}}
    {{--    <h2>Import Excel File into MySQL Database using PHP</h2>--}}

    {{--    <div class="outer-container">--}}
    {{--      <form action="xlsx/show" method="post"--}}
    {{--            name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">--}}
    {{--        <div>--}}
    {{--          <label>Choose Excel--}}
    {{--            File</label> <input type="file" name="file"--}}
    {{--                                id="file" accept=".xls,.xlsx">--}}
    {{--          <button type="submit" id="submit" name="import"--}}
    {{--                  class="btn-submit">Import</button>--}}

    {{--        </div>--}}

    {{--      </form>--}}

    {{--    </div>--}}
    {{--    <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>--}}
    {{--  </main>--}}
@endsection




