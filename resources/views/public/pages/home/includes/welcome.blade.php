<section class="welcome">
    <div class="container">
        <div class="flex_sb">
            <div class="welcome_left">
                <h1>{{ getTranslate('home.welcome.title', 'Собери подарок на любой вкус') }}</h1>
                <p>{{ getTranslate('home.welcome.text') }}</p>
                @include('public.layout.buttons.choose')
                <nav class="welcome-contacts">
                    <li><a href="{{ getSetting('social.facebook') }}" target="_blank" rel="nofollow">Facebook</a></li>
                    <li><a href="{{ getSetting('social.instagram') }}" target="_blank" rel="nofollow">Instagram</a></li>
                </nav>
            </div>
            <div class="welcome_right"><img class="lollipops" src="/img/lollipops.jpg" alt="Леденцы">
            </div>
            <div class="welcome_img_mob">
                <div class="wrap"><img class="lollipops" src="/img/lollipops-mob.png" alt="Леденцы">
                </div>
            </div>
        </div>
    </div>
</section>