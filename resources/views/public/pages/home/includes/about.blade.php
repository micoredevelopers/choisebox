<section class="about-us">
    <div class="container">
        <div class="flex_sb">
            <div class="about-us_left"><h2>{{getTranslate('home.about.title', 'О компании')}}</h2>
                <p>{{getTranslate('home.about.text1', 'Современные технологии достигли такого уровня, что реализация намеченных плановых заданий позволяет выполнить важные задания по разработке вывода текущих активов. Но укрепление и развитие внутренней структуры не даёт нам иного выбора, кроме определения системы обучения кадров, соответствующей насущным потребностям.')}}</p>
                <p>{{getTranslate('home.about.text2', 'С учётом сложившейся международной обстановки, дальнейшее развитие различных форм                    деятельности предопределяет высокую востребованность модели развития! Идейные соображения высшего порядка, а также реализация намеченных плановых заданий напрямую зависит от кластеризации усилий.')}}</p></div>
            <div class="about-us_right"><img src="{{ asset('/img/boxes.png') }}" alt="Коробки"></div>
        </div>
    </div>
    <div class="container advantage"><h2>{{getTranslate('home.about.advantage.title', 'Преимущества')}}</h2>
        <div class="flex_sb about-us_cards">
            <div class="about-us_card"><img src="{{ asset('/img/cardCandys.png') }}" alt="Иконка две конфеты">
                <h3>{{getTranslate('home.about.advantage.title1', 'Индивидуальные наборы')}}</h3>
                <p>{{getTranslate('home.about.advantage.text1', 'Современные технологии достигли такого уровня, что реализация намеченных плановых заданий позволяет выполнить важные задания по разработке вывода текущих активов.')}}</p></div>
            <div class="about-us_card"><img src="{{ asset('/img/cardtopProducers.png') }}"
                                            alt="Известные проиводители">
                <h3>{{getTranslate('home.about.advantage.title2', 'Известные производители')}}</h3>
                <p>{{getTranslate('home.about.advantage.text2', 'Современные технологии достигли такого уровня, что реализация намеченных плановых заданий позволяет выполнить важные задания по разработке вывода текущих активов.')}}</p></div>
            <div class="about-us_card"><img src="{{ asset('/img/cardhandCandy.png') }}" alt="Иконка рука с конфетой">
                <h3>{{getTranslate('home.about.advantage.title3', 'Сборка набора самому')}}</h3>
                <p>{{getTranslate('home.about.advantage.text3', 'Современные технологии достигли такого уровня, что реализация намеченных плановых заданий позволяет выполнить важные задания по разработке вывода текущих активов.')}}</p></div>
        </div>
    </div>
    <div class="container work"><h2>{{getTranslate('home.about.work.title', 'Как это работает?')}}</h2>
        <div class="flex_sb about-us_cards mod">
            <div class="about-us_card mod"><span>1</span>
                <h3>{{getTranslate('home.about.work.title1', 'Выбираете конфеты')}}</h3>
                <p>{{getTranslate('home.about.work.text1', 'Современные технологии достигли такого уровня, что реализация намеченных плановых заданий позволяет выполнить важные задания по разработке вывода текущих активов.')}}</p></div>
            <div class="about-us_card mod"><span>2</span>
                <h3>{{getTranslate('home.about.work.title2', 'Выбираете конфеты')}}</h3>
                <p>{{getTranslate('home.about.work.text2', 'Современные технологии достигли такого уровня, что реализация намеченных плановых заданий позволяет выполнить важные задания по разработке вывода текущих активов.')}}</p></div>
            <div class="about-us_card mod"><span>3</span>
                <h3>{{getTranslate('home.about.work.title3', 'Выбираете конфеты')}}</h3>
                <p>{{getTranslate('home.about.work.text3', 'Современные технологии достигли такого уровня, что реализация намеченных плановых заданий позволяет выполнить важные задания по разработке вывода текущих активов.')}}</p></div>
            <div class="about-us_card mod"><span>4</span>
                <h3>{{getTranslate('home.about.work.title4', 'Выбираете конфеты')}}</h3>
                <p>{{getTranslate('home.about.work.text4', 'Современные технологии достигли такого уровня, что реализация намеченных плановых заданий позволяет выполнить важные задания по разработке вывода текущих активов.')}}</p></div>
        </div>
    </div>
</section>