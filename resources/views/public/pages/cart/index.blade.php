@extends('public.layout.app')
@section('content')
    <main class="content-wrapper" id="orderPage">
        @include('public.pages.cart.includes.main')
    </main>
    @include('public.layout.includes.modals.search')
    @include('public.layout.includes.modals.menu')
    @include('public.layout.includes.modals.cart')
@endsection
