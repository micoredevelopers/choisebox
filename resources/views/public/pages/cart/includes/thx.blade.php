@extends('public.layout.app')
@section('content')
    <section id="thanks" class="thanksPage">
        <div class="thanksPage__content">
            <div class="thanksPage__content-img">
                <img src="{{ asset('img/thxLogo.png') }}" alt="thank you">
            </div>
            <h2 class="thanksPage__content-title">Спасибо за заказ!</h2>
            @isset($order)
                <p class="thanksPage__content-subtitle">Номер вашего заказа <b>{{ $order->getKey() }}</b></p>
            @endisset
            <p class="thanksPage__content-subtitle">Наш менеджер скоро с вами свяжется</p>
            <a class="thanksPage__content-btn button_type-one" href="{{ route('home') }}">Понятно</a>
        </div>
    </section>
@endsection
