<?php

/** @var $deliveryTypes \App\Enum\OrderDeliveryTypeEnum[] */
/** @var $paymentTypes \App\Enum\OrderPaymentTypeEnum[] */
/** @var $pickupTypes \App\Enum\OrderPickupPointTypeEnum[] */
/** @var $products \App\Models\Product\Product[]|\Illuminate\Support\Collection> */
/** @var $cartData \App\DataContainers\Cart\CartData */
?>
@if ($products->isNotEmpty())
    <section class="backet-page">
        <div class="container">
            <div class="backet-page_wrap left">
                <div class="title_wrap">
                    <div class="wrap flex_sb">
                        <h1>Корзина</h1>
                        <div class="product-block-mob"><a href="{{ route('category.show','boxes') }}" target="_blank">Варианты коробок</a></div>
                    </div>
                    <p>Конечная стоимость формируется после связи с менеджером</p>
                </div>
                <form action="{{route('order.create')}}" method="post" name="create_order" id="create_order">
                    @csrf
                    <div class="delivery-data">
                        <p class="delivery-data_title">Личные данные</p>
                        <div class="input-wrapper">
                            <div class="wrap">
                                <label for="name">Имя*</label>
                                <input required="required" class="input-type-one" id="name" placeholder="Имя"
                                       name="name"
                                       value="{{ old('name', getTestField('name'))  }}">
                            </div>
                            {!! errorDisplay('name') !!}
                            <div class="wrap">
                                <label for="surname">Фамилия*</label>
                                <input required="required" class="input-type-one" id="surname" placeholder="Фамилия"
                                       name="surname"
                                       value="{{ old('surname', getTestField('surname')) }}">
                            </div>
                            {!! errorDisplay('surname') !!}
                            <div class="wrap">
                                <label for="phone">Телефон*</label>
                                <input required="required" class="input-type-one imaskjs__input_tel" id="phone" type="tel"
                                       placeholder="+38 (ХХХ) ХХХ ХХ ХХ"
                                       name="phone" value="{{ old('phone', getTestField('phone')) }}">
                                {!! errorDisplay('phone') !!}
                            </div>
                            <div class="wrap">
                                <label for="email">Почта*</label>
                                <input required="required" class="input-type-one" id="email"
                                       value="{{ old('email', getTestField('email')) }}" placeholder="mail@gmail.com"
                                       name="email">
                                {!! errorDisplay('email') !!}
                            </div>
                        </div>
                    </div>
                    <div class="delivery-data">
                        <p class="delivery-data_title">Доставка</p>
                        <div class="input-wrapper">
                            <div class="radio_wrap" data-deliveryForm>

                                @foreach($deliveryTypes as $deliveryType)
                                    <div class="input_wrap">
                                        <input required="required" class="custom-radio" type="radio" autocomplete="off"
                                               id="{{ $deliveryType->getKey() }}" value="{{ $deliveryType->getKey() }}"
                                               name="delivery_type">
                                        <label for="{{ $deliveryType->getKey() }}">{{ $deliveryType->getTitle() }}</label>
                                    </div>
                                @endforeach
                                {!! errorDisplay('delivery_type') !!}
                                <div class="input-wrapper">
                                    {{-- @if($deliveryType->isNovaposhta())--}}
                                    <div class="inputs_section"
                                         data-delivery="{{ \App\Enum\OrderDeliveryTypeEnum::NOVAPOSHTA }}">
                                        <div class="wrap">
                                            <label for="city">Город</label>
                                            <input required="required" class="input-type-one" id="city"
                                                   placeholder="Одесса" name="city"
                                                   value="{{ old('city', getTestField('city')) }}">
                                        </div>
                                        <div class="wrap">
                                            <label for="branchnum">Номер отделения</label>
                                            <input required="required" class="input-type-one" id="branchnum"
                                                   placeholder="1" name="branch"
                                                   value="{{ old('branch', getTestField('branch')) }}">
                                        </div>
                                    </div>
                                    {{-- @elseif($deliveryType->isDelivery())--}}
                                    <div class="inputs_section"
                                         data-delivery="{{ \App\Enum\OrderDeliveryTypeEnum::DELIVERY }}">

                                        <div class="wrap">
                                            <label for="city">Город</label>
                                            <input required="required" class="input-type-one" id="city"
                                                   placeholder="Одесса" name="city"
                                                   value="{{ old('city', getTestField('city')) }}">
                                        </div>
                                        <div class="wrap">
                                            <label for="address">Адрес</label>
                                            <input required="required" class="input-type-one" id="address"
                                                   placeholder="ул. Пушкина 1а"
                                                   name="address" value="{{ old('address', getTestField('address')) }}">
                                        </div>
                                    </div>
                                    {{-- @elseif($deliveryType->isPersonalTransport())--}}
                                    <div class="inputs_section mod"
                                         data-delivery="{{ \App\Enum\OrderDeliveryTypeEnum::PERSONAL_TRANSPORT }}">
                                        <div class="input-wrapper">
                                            <div class="wrap">
                                                <label for="city">Город</label>
                                                <input required="required" class="input-type-one" id="city"
                                                       placeholder="Одесса" name="city"
                                                       value="{{ old('city', getTestField('city')) }}">
                                            </div>
                                            <div class="wrap">
                                                <label for="address">Адрес*</label>
                                                <input required="required" class="input-type-one" id="address"
                                                       placeholder="ул. Пушкина 1а"
                                                       name="address"
                                                       value="{{ old('address', getTestField('address')) }}">
                                            </div>
                                        </div>
                                        <p>
                                            <img src='/img/exclamation.svg'>
                                            От 1000 шт в Одесскую и Николаевские области
                                        </p>
                                    </div>
                                    {{-- @elseif($deliveryType->isPickUp())--}}
                                    <div class="inputs_section"
                                         data-delivery="{{ \App\Enum\OrderDeliveryTypeEnum::PICKUP }}">
                                        <div class="wrap mod">
                                            <label for="pickup_point">Пункт выдачи*</label>
                                            <select class="form-select input-type-one" id="pickup_point"
                                                    name="pickup_point" required="required"
                                                    aria-label="Default select example">
                                                <option selected>Выберите пункт выдачи</option>
                                                @foreach($pickupTypes as $pickupType)
                                                    <option value="{{ $pickupType->getKey() }}" {!! selectedIfTrue(1===count($pickupTypes)) !!}>{{ $pickupType->getTitle() }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="delivery-data">
                            <p class="delivery-data_title">Оплата</p>
                            @foreach($paymentTypes as $paymentType)
                                <div class="input-wrapper">
                                    <div class="radio_wrap">
                                        <div class="input_wrap">
                                            <input required="required" class="custom-radio" type="radio"
                                                   autocomplete="off" id="{{$paymentType->getKey()}}"
                                                   name="payment_type" value="{{ $paymentType->getKey() }}"
                                                    {!! checkedIfTrue($paymentType->getKey() === old('payment_type')) !!}>
                                            <label for="{{$paymentType->getKey()}}">{{$paymentType->getTitle()}}</label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            {!! errorDisplay('payment_type') !!}
                        </div>
                        <div class="delivery-data">
                            <div class="wrap mod">
                                <label for="comment">Комментарий</label>
                                <textarea class="input-type-one textarea" id="comment"
                                          placeholder="Есть дополнительная информация, которую нам нужно знать?"
                                          name="comment">{{ old('comment', getTestField('comment')) }}</textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="backet_sticky-wrap">
                <div class="backet-page_wrap right">
                    <div class="title flex_sb product-block-des">
                        <p>Заказ</p>
                        <a href="{{route('category.show','boxes')}}" target="_blank">Выбрать упаковку</a>
                    </div>
                    <div class="product-block product-block-des">
                        <div class="products-wrapper">
                            @foreach($cartData->getProducts() as $cartProduct)
                                <div class="product-wrap" data-productItem>
                                    <div class="product_img-wrap">
                                        <img alt="" src="{{ getPathToImage($cartProduct->getProduct()->image)}}">
                                    </div>
                                    <div class="product_title-wrapper">
                                        <div class="product_title-wrap product_title-wrap_cart">
                                            <p class="prod-title ">
                                                {{ $cartProduct->getProduct()->getProductName()}}
                                            </p>
                                            <div class="prod-modal">{{ $cartProduct->getProduct()->getProductName()}}</div>
                                            <p class="product_price">{{ $cartProduct->getProduct()->getPrice()}} {{ getCurrencyIcon() }}
                                                @if($cartProduct->getProduct()->getProductTypeEnum()->isBox())
                                                    <span>{{$cartProduct->getProduct()->getCapacity()}}</span>
                                                @else
                                                    <span>{{$cartProduct->getProduct()->getWeight()}} г</span>
                                                @endif
                                            </p>
                                        </div>
                                        <div class="product_number-wrap" data-form>
                                            <button class="button_type-one"
                                                    data-minusProduct="{{ route('cart.change',['id' => $cartProduct->getProduct()->getKey()]) }}">
                                                <img src='/img/minus.svg'>
                                            </button>
                                            <input  min="1" class="mx-1 formInput" type="number"
                                                   value="{{ $cartProduct->getQuantity() }}"
                                                    data-cart-amount-product-id="{{ $cartProduct->getProduct()->getKey() }}"
                                                    data-input="{{route('cart.change', ['id' => $cartProduct->getProduct()->getKey()])}}">

                                            <button class="button_type-one"
                                                    data-plusProduct="{{ route('cart.change',['id' => $cartProduct->getProduct()->getKey()]) }}">
                                                <img src='/img/plus.svg'>
                                            </button>
                                        </div>
                                        <button data-deleteUrl="{{ route('cart.remove', ['id' => $cartProduct->getProduct()->id]) }}"
                                                class="button_type-none deleteBtn"><img alt=""
                                                                                        src="/img/delete.svg">
                                        </button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
{{--                    <div class="wrap boxes product-block-des" data-countBoxes>--}}
{{--                        <p>Количество коробок</p>--}}
{{--                        <div class="product_number-wrap">--}}
{{--                            <button class="button_type-one" data-deleteBox>--}}
{{--                                <img src='/img/minus.svg'>--}}
{{--                            </button>--}}
{{--                            <input readonly="" value="{{ old('box_amount', 1) }}"--}}
{{--                                   type="number"--}}
{{--                                   name="box_amount" id="box_amount" data-boxInput--}}
{{--                                   form="create_order">--}}
{{--                            <button class="button_type-one" data-addBox>--}}
{{--                                <img src='/img/plus.svg'>--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="wrap bottom">
                        <div class="basket_total-title">
                            <p class="title-total">
                                <span class="product-block-des">Всего:</span>
                                <span class="total-priсe">{{ $cartCalcData->getCartCost() }} {{ getCurrencyIcon() }}</span>
                                <span class="total-weight" data-weight="{{ $cartCalcData->getCartWeight() }}"> {{ $cartCalcData->getCartWeight() }} г</span>
                            <p class="title-order"
                               data-minWeight="{{ (int)getSetting('order.minimal_amount_weight') }}">Минимальный
                                заказ 150 грамм</p>
                        </div>
                        <button data-orderBtn id="cartOrderBtn" class="button_type-one" form="create_order">Оформить
                            <span class="product-block-des">заказ</span>
                        </button>
                    </div>
                </div>
                <div class="emptyBlock"></div>
            </div>
        </div>
    </section>
@else
    <p>Ваша корзина пуста</p>
@endif