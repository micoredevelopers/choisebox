@extends('public.layout.app')
@section('content')
    <body class="index-page">

        <div class="content-wrapper">
            <div class="content-wrapper">
                @include('public.pages.products.main')
            </div>
            @include('public.layout.includes.modals.search')
            @include('public.layout.includes.modals.menu')
            @include('public.layout.includes.modals.cart')
    @include('public.layout.includes.forms.footer')

@endsection
