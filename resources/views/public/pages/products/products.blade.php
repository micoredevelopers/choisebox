<?php /** @var $cartData \App\DataContainers\Cart\CartData */ ?>
<?php /** @var $cartProduct \App\DataContainers\Cart\CartProductData */ ?>
<?php /** @var $product \App\Models\Product\Product */ ?>

<div class="prod-card" data-product-id="{{ $product->getKey() }}">
    <div class="prod-card_img-wrap">
        <img class="prod-card_img" src="{{ getPathToImage($product->getImage()) }}"
             alt="{{ $product->getProductName() }}">
    </div>
    <div class="wrap">
        <div class="prod-title brand-page_prod-title">
            <h2>{{$product->getProductName()}}</h2>
            <div class="prod-modal">
                {{$product->getProductName()}}
            </div>
        </div>

        {!! linkEditIfAdmin($product) !!}

        @if($product->getProductTypeEnum()->isBox())
            <div class="prod-card_text"><p class="mod">Размер: {{$product->getSize()}}</p></div>
            <div class="prod-card_text mod"><p class="mod">Ёмкость: {{$product->getCapacity()}}</p></div>
        @else
            <div class="prod-card_text brand-page_prod-card_text">
                <p>{{$product->getDescription()}}</p>
                <div class="prod-modal_text">{{$product->getDescription()}}</div>
            </div>
        @endif

        <div class="prod-card_bottom {{ $product->getProductTypeEnum()->isBox() ? 'mod' : '' }}">
            <p class="price-wrap">
                <span class="prace"> {{$product->getPrice()}} грн</span>
                @if(!$product->getProductTypeEnum()->isBox() && $product->getWeight())
                    <span class="weight"> {{$product->getWeight()}} г</span>
                @endif
            </p>
            <button data-inCart="{{ route('cart.add', ['id' => $product->getKey()]) }}"
                    data-product-id="{{ $product->getKey() }}"
                    class="button_type-one inCartBtn {{ $cartData->isProductInCart($product) ? 'dn' : '' }}">
                @includeIf('public.pages.products.includes.svgs.add_to_cart')
                <span>В корзину</span>
            </button>
            @php($cartProduct = $cartData->getProductCartDataByProduct($product))
            <div class="button_type-one inCartBtn inCartBtn-active active {{ $cartData->isProductInCart($product) ? '' : 'dn' }}">
                <button class="button_type-none"
                        data-minusProduct="{{ route('cart.change', $product->getKey()) }}">
                    @includeIf('public.pages.products.includes.svgs.minus_cart')
                </button>
                <div class="inCartBtn-active_input-wrap">
                    <input min="1" class="mx-1 formInput"
                           value="{{ $cartProduct ? $cartProduct->getQuantity() : 1 }}" type="number"
                           data-cart-amount-product-id="{{ $product->getKey() }}"
                           data-input="{{ route('cart.change', $product->getKey()) }}"><span>шт</span>
                </div>
                <button class="button_type-none"
                        data-plusProduct="{{ route('cart.change', $product->getKey()) }}">
                    @includeIf('public.pages.products.includes.svgs.plus_cart')
                </button>
            </div>
        </div>
    </div>
</div>