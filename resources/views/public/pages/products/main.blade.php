<?php /** @var $products \Illuminate\Pagination\LengthAwarePaginator|\App\Models\Product\Product[] */ ?>
<?php /** @var $sortEnums \App\Enum\ProductSortEnums[] */ ?>
<?php /** @var $cartData \App\DataContainers\Cart\CartData */ ?>
<?php /** @var $category \App\Models\Category\Category */ ?>
<?php /** @var $typeEnums \App\Enum\ProductTypeEnum */ ?>

@php
    $categoryName = ($category ?? false) ? $category->getCategoryName() : '';
@endphp
<section class="brand-head brand-page">
    <div class="container">
        <div class="title">
            <div class="title_wrap">
                <h1>{{ $categoryName }}</h1>
                <p class="mod">Найдено: <span>{{ $products->total() }}</span>
                    @foreach($typeEnums as $enum)
                        @if($category && $enum == $category->getProductType())
                            {{$enum->getTitle()}}
                        @endif
                    @endforeach
                </p>
            </div>
            <div class="title_wrap" id="sortForm">
                <p>Сортировка по</p>
                <select class="form-select mod" aria-label="Default select example" name="sort" form="searchForm"
                        onchange="searchForm.submit()" autocomplete="off">
                    @foreach($sortEnums as $sortEnum)
                        <option value="{{ $sortEnum->getKey() }}"
                                title="{{ $sortEnum->getTitle() }}"
                                {!! selectedIfTrue(request('sort', '') === $sortEnum->getKey()) !!}
                        >{{ $sortEnum->getTitle() }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="prod-wrap">
            @if($products->isNotEmpty())
                @foreach($products as $product)
                    @include('public.pages.products.products')
                @endforeach
            @endif
        </div>
        {!! $products->onEachSide(0)->render() !!}
    </div>
</section>