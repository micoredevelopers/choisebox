import {CustomSelect} from "./utils/custom-select";
import { basket } from "./utils/basket";
import {menu} from "./utils/header";
import {orderScript} from "./utils/order";

document.addEventListener('DOMContentLoaded', function() {
	menu();
	basket();
	var elements = document.getElementsByClassName('imaskjs__input_tel');
		for (var i = 0; i < elements.length; i++) {
		new IMask(elements[i], {
			mask: '+{38}(000)000-00-00',
		});
	}

	const productTitle = document.querySelectorAll(".brand-page_prod-title");
	
	productTitle.forEach(text => {
		const productTitleText = text.childNodes[1].textContent.length;
		const productTitleTutor = text.childNodes[3].classList;
		text.addEventListener("mouseover", function(e) {
			if(productTitleText > 49){
				productTitleTutor.add("prod-modal_active")
			}
		});
		text.addEventListener("mouseout", function(e) {
			if(productTitleText > 49){
				productTitleTutor.remove("prod-modal_active")
			}
		});
	});

	const productText = document.querySelectorAll(".brand-page_prod-card_text");

	productText.forEach(text => {
		const productTextText = text.childNodes[1].textContent.length;
		const productTextTutor = text.childNodes[3].classList;
		text.addEventListener("mouseover", function(e) {
			if(productTextText > 115){
				productTextTutor.add("prod-modal_active")
			}
		});
		text.addEventListener("mouseout", function(e) {
			if(productTextText > 115){
				productTextTutor.remove("prod-modal_active")
			}
		});
	});

	const productCartTitle = document.querySelectorAll(".product_title-wrap_cart");

	productCartTitle.forEach(text => {
		const productCartText = text.childNodes[1].textContent.length;
		const productCartTutor = text.childNodes[3].classList;
		text.addEventListener("mouseover", function(e) {
			if(productCartText > 135){
				productCartTutor.add("prod-modal_active")
			}
		});
		text.addEventListener("mouseout", function(e) {
			if(productCartText > 135){
				productCartTutor.remove("prod-modal_active")
			}
		});
		// const mediaQuery = window.matchMedia('(min-width: 500px)')
		// if(mediaQuery.matches){
		// 	// text.addEventListener("mouseover", function(e) {
		// 	// 	if(productCartText > 15){
		// 	// 		productCartTutor.add("prod-modal_active")
		// 	// 	}
		// 	// });
		// 	// text.addEventListener("mouseout", function(e) {
		// 	// 	if(productCartText > 15){
		// 	// 		productCartTutor.remove("prod-modal_active")
		// 	// 	}
		// 	// });
		// }
	});

	const selectId = document.getElementById("select-1");
	const select2Id = document.getElementById("select-2");

	if(selectId) {
			const select = new CustomSelect(selectId);
	}
	if(select2Id) {
			const select2 = new CustomSelect(select2Id)
	}

	const pageId = document.querySelector('main')?.id;

	switch (pageId) {
		case 'orderPage':
			orderScript()
					break
		default:
			break
	}

})
