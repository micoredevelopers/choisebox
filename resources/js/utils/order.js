
export const orderScript = () => {
  // window.scroll(function () {
  //   alert("ssss")
  //   const menuHeader = document.querySelector('#header')
  //   const a = 99;
  //   const pos = window.scrollX();
  //   if (pos > a) {
  //     menuHeader.classList.add("scroll-style");
  //   }
  //   else {
  //     menuHeader.classList.remove("scroll-style");
  //   }
  // });
  //order form
  const deliveryForm = document.querySelector('[data-deliveryForm]')
  const deliveryRadio = deliveryForm.querySelectorAll('[name="delivery_type"]')

  
	

  deliveryRadio.forEach(radio => {
    radio.addEventListener('change', e => {
      const deliveryInputContainer = deliveryForm.querySelectorAll(`[data-delivery]`)
      const selectInputContainer = deliveryForm.querySelector(`[data-delivery="${radio.id}"]`)
      deliveryInputContainer.forEach(inputWrap => {
        inputWrap.classList.remove('visibility')
        const inputs = inputWrap.querySelectorAll('.wrap > input')
        inputs.forEach(input => {
          input.setAttribute('disabled', 'disabled')
        })
      })
      selectInputContainer.classList.add('visibility')
      const inputs = selectInputContainer.querySelectorAll('.wrap > input')
      inputs.forEach(input => {
        input.removeAttribute('disabled')
      })
    })
  })
}