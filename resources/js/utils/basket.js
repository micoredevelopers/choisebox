import axios from "axios";

export const basket = () => {

  const productForm = document.querySelectorAll('.product-block');
  const boxCount_form = document.querySelectorAll('[data-countBoxes]');

  const inCart_btn = document.querySelectorAll('[data-inCart]');
  const headerPrice = document.querySelector('.cartModal-btn > span');
  const headerCount = document.querySelectorAll('.cartModal-btn > .indicator');

  const totalPriceElement = document.querySelectorAll('.total-priсe');
  const totalWeightElement = document.querySelectorAll('.total-weight');

  const minWeight = document.querySelector('[data-minWeight]').getAttribute('data-minWeight');
  const orderBtn = document.querySelector('[data-orderBtn]')

  const onDisabledOrderBtn = (weight) => {
    if (+minWeight > +weight) {
      orderBtn.classList.add('disable');
    } else if (+minWeight <= +weight) {
      orderBtn.classList.remove('disable');
    }
  }

  const changeAmountCartInputs = (productId, amount, parentClassName) => {
    const inputs = document.querySelectorAll(`[data-cart-amount-product-id="${productId}"]`);
    inputs.forEach(input => {
      const parent = input.closest(parentClassName);
      if (!!parent) {
        const minusBtn = parent.querySelector('[data-minusProduct]');
        if (+input.value <= 1) {
          minusBtn.classList.add('disable')
          minusBtn.setAttribute('disable', true);
        }
        if (+input.value > 1) {
          minusBtn.classList.remove('disable')
          minusBtn.removeAttribute('disabled')
        }
      }
      input.value = amount
    })
  }

  const onChange = {
    handleMinus: (input, minusBtn, minusUrl, totalPriceElement, totalWeightElement) => e => {
      if (input.value <= 2) {
        minusBtn.classList.add('disable')
        minusBtn.setAttribute('disabled', true);
      }
      const value = input.value;
      axios.post(minusUrl, {count: (+value - 1)}).then(({data}) => {
        totalPriceElement.forEach(price => price.innerHTML = `${data.cartCost} грн`)
        totalWeightElement.forEach(weight => weight.innerHTML = `${data.cartWeight} г`)
        onDisabledOrderBtn(data.cartWeight)
        changeAmountCartInputs(input.getAttribute('data-cart-amount-product-id'), +value - 1);
      })
    },
    handlePlus: (input, minusBtn, plusUrl, totalPriceElement, totalWeightElement) => e => {
      const value = input.value;
      axios.post(plusUrl, {count: (+value + 1)}).then(({data}) => {
        changeAmountCartInputs(input.getAttribute('data-cart-amount-product-id'), +value + 1);
        totalPriceElement.forEach(price => price.innerHTML = `${data.cartCost} грн`)
        totalWeightElement.forEach(weight => weight.innerHTML = `${data.cartWeight} г`)
        if (input.value > 1) {
          minusBtn.classList.remove('disable')
          minusBtn.removeAttribute('disabled');
        }
        onDisabledOrderBtn(data.cartWeight)
      })
    },
    handleFocusInput: e => e.currentTarget.setAttribute('data-previousValue', e.target.value),
    handleBlurInput: (minusBtn, totalPriceElement, totalWeightElement) => e => {
      let value = e.target.value;
      const requestUrl = e.currentTarget.getAttribute('data-input');
      const productID = e.currentTarget.getAttribute('data-cart-amount-product-id');

      if (!!value && value > 0) {

        if (value < 2) {
          minusBtn.classList.add('disable')
          minusBtn.setAttribute('disabled', true);
        } else {
          minusBtn.classList.remove('disable')
          minusBtn.removeAttribute('disabled');
        }

        axios.post(requestUrl, {count: +value}).then(({data}) => {
          totalPriceElement.forEach(price => price.innerHTML = `${data.cartCost} грн`);
          totalWeightElement.forEach(weight => weight.innerHTML = `${data.cartWeight} г`);
          headerCount.forEach(indicator => indicator.innerHTML = `${data.totalProducts}`)
          headerPrice.innerHTML = `${data.cartCost} грн`;
          changeAmountCartInputs(productID, +value, '.product_number-wrap');
          onDisabledOrderBtn(data.cartWeight)
        })
      } else {
        const prevValue = e.target.getAttribute('data-previousValue');
        return e.target.value = prevValue;
      }
    },
    handleDelete: (item, itemId, deleteUrl, totalPriceElement, totalWeightElement) => e => {
      const activeMenuBtn = document.querySelector(`button[data-product-id="${itemId}"]`);
      const activeInputBtn = activeMenuBtn?.nextElementSibling;
      axios.post(deleteUrl).then(({data}) => {
        onDisabledOrderBtn(data.cartWeight)
        item.remove();
        totalPriceElement.forEach(price => price.innerHTML = `${data.cartCost} грн`)
        totalWeightElement.forEach(weight => weight.innerHTML = `${data.cartWeight} г`)
        headerCount.forEach(indicator => indicator.innerHTML = `${data.totalProducts}`)
        headerPrice.innerHTML = `${data.cartCost} грн`;
        activeMenuBtn?.classList.remove('dn');
        if (!!activeInputBtn) {
          activeInputBtn.classList.add('dn');
        }

      }).catch((e) => {
        console.log(e)
      })
    }
  }

  const addClickListener = (form) => {
    const productItem = form.querySelectorAll('[data-productItem]');

    totalWeightElement.forEach(weight => {
      if (+minWeight > +weight.getAttribute('data-weight')) {
        orderBtn.classList.add('disable');
      }
    })

    productItem.forEach(item => {
      const deleteBtn = item.querySelector('[data-deleteUrl]')
      const itemId = deleteBtn.getAttribute('data-product-id');
      const deleteUrl = deleteBtn.getAttribute('data-deleteUrl');
      const minusBtn = item.querySelector('[data-minusProduct]');
      const plusBtn = item.querySelector('[data-plusProduct]');
      const input = item.querySelector('[data-input]');
      const minusUrl = minusBtn.getAttribute('data-minusProduct');
      const plusUrl = plusBtn.getAttribute('data-plusProduct');

      if (input.value <= 1) {
        minusBtn.classList.add('disable')
        minusBtn.setAttribute('disabled', true);
      }

      minusBtn.addEventListener('click', onChange.handleMinus(input, minusBtn, minusUrl, totalPriceElement, totalWeightElement))
      plusBtn.addEventListener('click', onChange.handlePlus(input, minusBtn, plusUrl, totalPriceElement, totalWeightElement))
      deleteBtn.addEventListener('click', onChange.handleDelete(item, itemId, deleteUrl, totalPriceElement, totalWeightElement))
      input.addEventListener('focus', onChange.handleFocusInput)
      input.addEventListener('blur', onChange.handleBlurInput(minusBtn, totalPriceElement, totalWeightElement))
    })
  }

  //Boxes count form
  // boxCount_form.forEach(boxWrap => {
  //   const minusBoxBtn = boxWrap.querySelector('[data-deleteBox]');
  //   const plusBoxBtn = boxWrap.querySelector('[data-addBox]');
  //   const boxInput = boxWrap.querySelector('[data-boxInput]');
  //
  //   const onChangeBoxCount = (element, attribute) => {
  //     switch (attribute) {
  //       case 'data-deleteBox':
  //         if (boxInput.value <= 2) {
  //           minusBoxBtn.classList.add('disable')
  //           minusBoxBtn.setAttribute('disabled', true);
  //         }
  //         boxInput.value = +boxInput.value - 1;
  //         break
  //       case 'data-addBox':
  //         boxInput.value = +boxInput.value + 1;
  //         if (boxInput.value > 1) {
  //           minusBoxBtn.classList.remove('disable')
  //           minusBoxBtn.removeAttribute('disabled');
  //         }
  //         break
  //       default:
  //         break
  //     }
  //   }
  //
  //   if (boxInput.value <= 1 || boxInput.value == 1) {
  //     minusBoxBtn.classList.add('disable')
  //     minusBoxBtn.setAttribute('disabled', true);
  //   }
  //
  //   minusBoxBtn?.addEventListener('click', e => {
  //     onChangeBoxCount(e.currentTarget, 'data-deleteBox');
  //   })
  //
  //   plusBoxBtn.addEventListener('click', e => {
  //     onChangeBoxCount(e.currentTarget, 'data-addBox');
  //   })
  // })

  const productList = document.querySelector('[data-productList]')

  //add in cart
  inCart_btn.forEach(btn => {
    const inCart_btn_Active = btn.nextElementSibling;

    const input = inCart_btn_Active.querySelector('[data-input]');
    const minus = inCart_btn_Active.querySelector('[data-minusProduct]');
    const plus = inCart_btn_Active.querySelector('[data-plusProduct]');

    inCart_btn_Active.addEventListener('click', e => {
      const value = input.value;

      if (input.value > 1) {
        minus.classList.remove('disabled')
        minus.removeAttribute('disabled');
      }

      if (e.target.closest('[data-plusProduct]') === plus) {
        axios.post(plus.getAttribute('data-plusProduct'), {count: +value + 1}).then(({data}) => {
          changeAmountCartInputs(input.getAttribute('data-cart-amount-product-id'), +value + 1, '.product_number-wrap');
          headerPrice.innerHTML = `${data.cartCost} грн`;
          headerCount.forEach(indicator => indicator.innerHTML = `${data.totalProducts}`)
          totalPriceElement.forEach(price => price.innerHTML = `${data.cartCost} грн`)
          totalWeightElement.forEach(weight => weight.innerHTML = `${data.cartWeight} г`)
          onDisabledOrderBtn(data.cartWeight)
        })
      } else if (e.target.closest('[data-minusProduct]') === minus) {
        if (input.value <= 2) {
          minus.classList.add('disabled')
          minus.setAttribute('disabled', true);
        }
        axios.post(minus.getAttribute('data-minusProduct'), {count: +value - 1}).then(({data}) => {
          changeAmountCartInputs(input.getAttribute('data-cart-amount-product-id'), +value - 1, '.product_number-wrap');
          headerPrice.innerHTML = `${data.cartCost} грн`;
          headerCount.forEach(indicator => indicator.innerHTML = `${data.totalProducts}`)
          totalPriceElement.forEach(price => price.innerHTML = `${data.cartCost} грн`)
          totalWeightElement.forEach(weight => weight.innerHTML = `${data.cartWeight} г`)
          onDisabledOrderBtn(data.cartWeight)
        })
      }
    })

    input.addEventListener('focus', onChange.handleFocusInput)
    input.addEventListener('blur', e => {
      let value = e.target.value;
      const requestUrl = e.currentTarget.getAttribute('data-input');

      if (!!value && value > 0) {
        axios.post(requestUrl, {count: +value}).then(({data}) => {
          headerPrice.innerHTML = `${data.cartCost} грн`;
          headerCount.forEach(indicator => indicator.innerHTML = `${data.totalProducts}`)
          totalPriceElement.forEach(price => price.innerHTML = `${data.cartCost} грн`);
          totalWeightElement.forEach(weight => weight.innerHTML = `${data.cartWeight} г`);
          onDisabledOrderBtn(data.cartWeight)
          changeAmountCartInputs(input.getAttribute('data-cart-amount-product-id'), +e.target.value, '.product_number-wrap');
        })
      } else {
        const prevValue = e.target.getAttribute('data-previousValue');
        return e.target.value = prevValue;
      }
    })

    btn.addEventListener('click', (e) => {
      const url = btn.getAttribute('data-inCart')
      axios.post(url).then(({data}) => {
        btn.classList.add('dn');
        inCart_btn_Active.classList.remove('dn');

        headerPrice.innerHTML = `${data.cartCost} грн`;
        headerCount.forEach(indicator => indicator.innerHTML = `${data.totalProducts}`)
        totalPriceElement.forEach(price => price.innerHTML = `${data.cartCost} грн`)
        totalWeightElement.forEach(weight => weight.innerHTML = `${data.cartWeight} г`)
        onDisabledOrderBtn(data.cartWeight)

        const div = document.createElement('div');
        div.className = 'product-wrap';
        div.setAttribute('data-productItem', '');
        div.innerHTML = `<div class="product_img-wrap">
                                <img alt="" src=${data.product.imagePath}>
                         </div>
                         <div class="product_title-wrapper">
                                <div class="product_title-wrap product_title-wrap_cart">
                                    <p>${data.product.name}</p>
                                    <div class="prod-modal">${data.product.name}</div>
                                    <p class="product_price">${data.product.price} грн
                                        <span>${data.product.weight} г</span>
                                    </p></div>
                                <div class="product_number-wrap" data-form>
                                    <button class="button_type-one"
                                            data-minusProduct="/cart/change/${data.product.id}">
                                        <img src='/img/minus.svg'>
                                    </button>
                                    <input  min="1" class="mx-1 formInput" type="number" value="1"
                                            data-input="/cart/change/${data.product.id}"
                                            data-cart-amount-product-id="${data.product.id}">
                                    <button class="button_type-one"
                                            data-plusProduct="/cart/change/${data.product.id}">
                                        <img src='/img/plus.svg'>
                                    </button>
                                </div>
                                <button data-product-id="${data.product.id}" data-deleteUrl="/cart/cart/remove/${data.product.id}"
                                        class="button_type-none deleteBtn"><img alt="" src="/img/delete.svg">
                                </button>
                         </div>`
        productList.appendChild(div)
        const deleteBtn = div.querySelector('[data-deleteUrl]')
        const itemId = deleteBtn.getAttribute('data-product-id');

        const deleteUrl = deleteBtn.getAttribute('data-deleteUrl');

        const minusBtn = div.querySelector('[data-minusProduct]');
        const plusBtn = div.querySelector('[data-plusProduct]');
        const input = div.querySelector('[data-input]');

        const minusUrl = minusBtn.getAttribute('data-minusProduct');
        const plusUrl = plusBtn.getAttribute('data-plusProduct');

        if (input.value <= 1 || input.value == 1) {
          minusBtn.classList.add('disable')
          minusBtn.setAttribute('disabled', true);
        }

        minusBtn.addEventListener('click', onChange.handleMinus(input, minusBtn, minusUrl, totalPriceElement, totalWeightElement))
        plusBtn.addEventListener('click', onChange.handlePlus(input, minusBtn, plusUrl, totalPriceElement, totalWeightElement))
        deleteBtn.addEventListener('click', onChange.handleDelete(div, itemId, deleteUrl, totalPriceElement, totalWeightElement))
        input.addEventListener('focus', onChange.handleFocusInput)
        input.addEventListener('blur', onChange.handleBlurInput(minusBtn, totalPriceElement, totalWeightElement))
      })
    })
  })

  //Product item form
  productForm.forEach(addClickListener)
}