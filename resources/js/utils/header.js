export const menu = () => {
  const body = document.querySelector('body');
  const header = document.querySelector('header');

  const closeMenuBtn = document.querySelectorAll('[data-closeMenu]');
  const buttonsMenu = document.querySelectorAll('.button-menu');
  const menu = document.getElementById('menu');
  const menuText = document.querySelector('.button-menu > span');

  const search = document.getElementById('search');
  const buttonsSearch = document.querySelectorAll('.button-search');

  const closeModalBtn = document.querySelectorAll('.basket-close');
  const cartModal = document.getElementById('basket-modal');
  const cartModalBtn = document.querySelectorAll('.cartModal-btn');


  window.addEventListener('scroll', function () {
    const y = window.scrollY
      if (y >= 100){
          header.classList.add("scroll-style")
      } else {
          header.classList.remove("scroll-style")
      }
  })


  closeMenuBtn.forEach(btn => {
    btn.addEventListener('click', e => {
      menu.classList.remove('active');
      body.classList.remove('lock');
      header.classList.remove('hightIndex');
      buttonsMenu.forEach(button => button.classList.remove('active'))
    })
  })

  buttonsMenu.forEach(button => {
    button.addEventListener('click', (e) => {
      buttonsMenu.forEach(btn => btn.classList.toggle('active'))
      console.log(button)
      menu.classList.toggle('active');
      body.classList.toggle('Menu-lock');
      header.classList.toggle('hightIndex');
      menuText.innerHTML = menuText.innerHTML === 'Выбрать конфеты'
        ? menuText.innerHTML = 'Закрыть меню'
        : menuText.innerHTML = 'Выбрать конфеты'
    });
  })

  buttonsSearch.forEach(button => {
    button.addEventListener('click', (e) => {
      button.classList.toggle('active')
      search.classList.toggle('active');
      body.classList.toggle('Search-lock');
      header.classList.toggle('hightIndex');
    });
  })

  search.addEventListener('click', e => {
    const {target, currentTarget} = e;
    if (currentTarget.className.includes('active') && target === currentTarget) {
      currentTarget.classList.remove('active');
      body.classList.remove('lock');
      header.classList.toggle('hightIndex');
      buttonsSearch.forEach(button => button.classList.remove('active'))
    }
  })

  cartModalBtn.forEach(button => {
    button.addEventListener('click', (e) => {
      cartModal.classList.add('active');
      body.classList.add('lock');
    });
  })

  cartModal.addEventListener('click', (e) => {
    const target = e.target
    const closeBtn = target.closest('.basket-close')
    if (target.className.includes('modal_bg') || closeBtn === closeModalBtn[0]) {
      cartModal.classList.remove('active');
      body.classList.remove('lock');
    }
  })

  const infoTextButton = document.querySelectorAll('.info-text_wrap_img');
  const infoTextButtonSecond = document.querySelectorAll('.info-text_wrap_img_second');

  const infoText = document.getElementById('info-text');
  const infoTextSecond = document.getElementById('info-text_second');


  infoTextButton.forEach(button => {
    button.addEventListener('click', (e) => {
      infoText.classList.toggle('act');
    });
  })
  infoTextButtonSecond.forEach(button => {
    button.addEventListener('click', (e) => {
      infoTextSecond.classList.toggle('act');
    });
  })
}
