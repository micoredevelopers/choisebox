<?php

namespace Database\Seeders;

use App\Models\Setting;
use App\Repositories\Admin\SettingsRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Prettus\Validator\Exceptions\ValidatorException;

class SettingsTableSeeder extends Seeder
{
	private $settings;
	/**
	 * @var SettingsRepository
	 */
	private $repository;

	public function __construct(SettingsRepository $repository)
	{
		$this->settings = $repository->withTrashed()->get()->keyBy('key');
		$this->repository = $repository;
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 * @throws ValidatorException
	 */
	public function run(): void
	{
		$groups = [
			'global' => [
				$this->getBuilder('sitename')->setValue('Dealok')->setDisplayName('Название сайта')->build(),
				$this->getBuilder('phone-one')->setValue('0 800 80 8080')->setDisplayName('Номер телефона #1')->build(),
				$this->getBuilder('phone-two')->setValue('+38 (063) 999 99 99')->setDisplayName('Номер телефона #2')->build(),
				$this->getBuilder('currency')->setValue('грн')->setDisplayName('Название валюты')->build(),
				$this->getBuilder('schedule.weekdays')->setValue('Пн — Пт: c 8:00 до 20:00')->setDisplayName('График работы будни')->build(),
				$this->getBuilder('schedule.weekend')->setValue('Сб — Вс: c 9:00 до 18:00')->setDisplayName('График работы выходные')->build(),
			],
			'email' => [
				$this->getBuilder('contact-email')->setValue(isLocalEnv() ? 'exceptions.manticore@gmail.com' : '')->setDisplayName('e-mail Администратора (для уведомлений)')->build(),
				$this->getBuilder('public-email')->setValue('')->setDisplayName('E-mail для контактов')->build(),
			],
			'social' => [
				$this->getBuilder('facebook')->setValue('')->setDisplayName('Facebook')->build(),
				$this->getBuilder('instagram')->setValue('')->setDisplayName('Instagram')->build(),
				$this->getBuilder('telegram')->setValue('')->setDisplayName('Telegram')->build(),
				$this->getBuilder('viber')->setValue('')->setDisplayName('Viber')->build(),
			],
			'order' => [
				$this->getBuilder('minimal_amount_weight')->setValue('150')->setDisplayName('Минимальный вес для заказа')->build(),
			],
			'_' => [
			],
		];
		$groups = array_reverse($groups);
		foreach ($groups as $groupName => $settings) {
			foreach ($settings as $setting) {
				$setting['group'] = $groupName;
				$setting['key'] = implode('.', [$groupName, $setting['key']]);
				if ($this->settingExists($setting['key'])) {
					continue;
				}
				$this->repository->create($setting);
			}
		}
	}

	private function getSettingByKey(string $key): ?Setting
	{
		return \Arr::get($this->settings, $key);
	}

	private function settingExists(string $key): bool
	{
		return (bool)$this->getSettingByKey($key);
	}

	private function getBuilder(?string $key)
	{
		$builder = new class implements \ArrayAccess {
			private $key = '';

			private $value = '';

			private $type = 'text';

			private $displayName = '';

			public function setKey(string $_): self
			{
				$this->key = $_;

				return $this;
			}

			public function setType(string $_): self
			{
				$this->type = $_;

				return $this;
			}

			public function setTypeTextarea(): self
			{
				$this->type = 'rich_text_box';

				return $this;
			}

			public function setTypeEditor(): self
			{
				$this->type = 'ckeditor';

				return $this;
			}

			public function setTypeFile(): self
			{
				$this->type = 'file';

				return $this;
			}

			public function setTypeCheckbox(): self
			{
				$this->setType('checkbox');

				return $this;
			}

			public function setValue(?string $_ = null): self
			{
				$this->value = $_;

				return $this;
			}

			public function setDisplayName(string $_): self
			{
				$this->displayName = $_;

				return $this;
			}

			public function build()
			{
				return [
					'key' => $this->key,
					'value' => $this->value,
					'type' => $this->type,
					'display_name' => $this->displayName,
				];
			}

			public function offsetExists($offset)
			{
				return property_exists($this, $offset);
			}

			public function offsetGet($offset)
			{
				return $this->offsetExists($offset) ? $this->{$offset} : null;
			}

			public function offsetSet($offset, $value)
			{
				$method = Str::camel('set' . ucfirst($offset));
				if (method_exists($this, $method)) {
					$this->$method($value);
				}
			}

			public function offsetUnset($offset)
			{
				if ($this->offsetExists($offset)) {
					$this->{$offset} = null;
				}
			}

		};
		if ($key) {
			$builder->setKey($key);
		}

		return $builder;
	}

}
