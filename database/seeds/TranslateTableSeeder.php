<?php

namespace Database\Seeders;

use App\Models\Translate\Translate;
use App\Repositories\TranslateRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TranslateTableSeeder extends Seeder
{
	private $translates;
	/**
	 * @var TranslateRepository
	 */
	private $repository;

	public function __construct(TranslateRepository $repository)
	{
		$this->translates = $repository->all()->keyBy('key');
		$this->repository = $repository;
	}

	public function run()
	{
		$groups = [
			'global' => [
				['key' => 'show-more', 'value' => 'Показать еще'],
				['key' => 'back', 'value' => 'назад'],
				['key' => 'yes', 'value' => 'Да'],
                ['key' => 'no', 'value' => 'Нет'],
                ['key' => 'asd', 'value' => 'hello %name%'],
				['key' => 'download', 'value' => 'Скачать'],
				['key' => '404', 'value' => 'Страница не найдена'],
				['key' => '404-description', 'value' => 'Страница не найдена, попробуйте другой адресс', 'type' => 'textarea'],
				'work-hours' => 'График работы:',
			],

			'forms' => [
				'fio'      => 'ФИО',
				'name'     => 'Имя',
				'phone'    => 'Номер телефона',
				'email'    => 'E-mail',
				'message'  => 'Комментарий',
				'send'     => 'Отправить заявку',
				'region'   => 'Область',
				'district' => 'Район',
				'city'     => 'Город',
				'password' => 'Пароль',
			],
			'users' => [
				$this->getBuilder('email')->setValue('E-mail')->build(),
				$this->getBuilder('fio')->setValue('ФИО')->build(),
			],

            'main' => [
                $this->getBuilder('menu.title')->setValue('Выбрать конфеты')->build(),

                $this->getBuilder('cart')->setValue('Корзина')->build(),
                $this->getBuilder('cart.total')->setValue('Всего')->build(),
                $this->getBuilder('cart.title')->setValue('Минимальный заказ 150 грамм')->build(),

                $this->getBuilder('form.title')->setValue('Остались вопросы?')->build(),
                $this->getBuilder('form.text')->setValue('Мы с удовольствием ответим на них. Оставьте ваши контактыи наш менеджер свяжется с вами в
                    ближайшее время.')->build(),

                $this->getBuilder('header.choose')->setValue('Выбрать конфеты')->build(),
                $this->getBuilder('header.search')->setValue('Поиск')->build(),

            ],

            'home' => [
                $this->getBuilder('welcome.title')->setValue('Подбери конфеты под свой вкус')->build(),
                $this->getBuilder('welcome.text')->setValue('Большой выбор конфет под любой вкус. Собери коробку для себя, на подарок или на любой другой
                    случай в жизни.
                    Ведь с конфетами всегда слаще, чем без них.
                    Попробуй подобрать прям сейчас!')->build(),

                $this->getBuilder('about.title')->setValue('О компании')->build(),
                $this->getBuilder('about.text1')->setValue('Современные технологии достигли такого уровня, что реализация намеченных плановых заданий
                    позволяет выполнить важные задания по разработке вывода текущих активов. Но укрепление и
                    развитие внутренней структуры не даёт нам иного выбора, кроме определения системы обучения
                    кадров, соответствующей насущным потребностям.')->build(),
                $this->getBuilder('about.text2')->setValue('С учётом сложившейся международной обстановки, дальнейшее развитие различных форм
                    деятельности предопределяет высокую востребованность модели развития! Идейные соображения
                    высшего порядка, а также реализация намеченных плановых заданий напрямую зависит от
                    кластеризации усилий.')->build(),

                $this->getBuilder('about.advantage.title')->setValue('Преимущества')->build(),
                $this->getBuilder('about.advantage.title1')->setValue('Индивидуальные наборы')->build(),
                $this->getBuilder('about.advantage.text1')->setValue('Современные технологии достигли такого уровня, что реализация намеченных плановых заданий
                    позволяет выполнить важные задания по разработке вывода текущих активов.')->build(),
                $this->getBuilder('about.advantage.title2')->setValue('Известные производители')->build(),
                $this->getBuilder('about.advantage.text2')->setValue('Современные технологии достигли такого уровня, что реализация намеченных плановых заданий
                    позволяет выполнить важные задания по разработке вывода текущих активов.')->build(),
                $this->getBuilder('about.advantage.title3')->setValue('Сборка набора самому')->build(),
                $this->getBuilder('about.advantage.text3')->setValue('Современные технологии достигли такого уровня, что реализация намеченных плановых заданий
                    позволяет выполнить важные задания по разработке вывода текущих активов.')->build(),

                $this->getBuilder('about.work.title')->setValue('Как это работает?')->build(),
                $this->getBuilder('about.work.title1')->setValue('Выбираете конфеты')->build(),
                $this->getBuilder('about.work.text1')->setValue('Современные технологии достигли такого уровня, что реализация намеченных плановых заданий
                    позволяет выполнить важные задания по разработке вывода текущих активов.')->build(),
                $this->getBuilder('about.work.title2')->setValue('Выбираете конфеты')->build(),
                $this->getBuilder('about.work.text2')->setValue('Современные технологии достигли такого уровня, что реализация намеченных плановых заданий
                    позволяет выполнить важные задания по разработке вывода текущих активов.')->build(),
                $this->getBuilder('about.work.title3')->setValue('Выбираете конфеты')->build(),
                $this->getBuilder('about.work.text3')->setValue('Современные технологии достигли такого уровня, что реализация намеченных плановых заданий
                    позволяет выполнить важные задания по разработке вывода текущих активов.')->build(),
                $this->getBuilder('about.work.title4')->setValue('Выбираете конфеты')->build(),
                $this->getBuilder('about.work.text4')->setValue('Современные технологии достигли такого уровня, что реализация намеченных плановых заданий
                    позволяет выполнить важные задания по разработке вывода текущих активов.')->build(),
            ],
            'button' => [
                $this->getBuilder('choose.menu')->setValue('Выбрать конфеты')->build(),
                $this->getBuilder('order')->setValue('Оформить заказ')->build(),
                $this->getBuilder('send')->setValue('Отправить')->build(),


            ],
			'pages' => [
				'about'    => 'О нас',
				'contacts' => 'Контакты',
			],

			'feedback'      => [
				'throttle-message'                 => 'Отправлять заявку можно не чаще 1 раза в минуту.',
				'send-success'                     => 'Заявка успешно отправлена',
				'send-failed'                      => 'Заявка не была отправлена, произошла ошибка на сервере, пожалуйста попробуйте позже, или позвоните нам по одному из номеров телефона',
				'name'                             => 'Имя',
				'email'                            => 'Почта',
			],
			'reviews'       => [
				'title' => 'Отзывы',
			],
		];
		foreach ($groups as $groupName => $group) {
			foreach ($group as $key => $item) {
				if (!is_array($item)) {
					$item = ['key' => $key, 'value' => $item];
				}
				$item['group'] = $groupName;
				$item['key'] = $this->addPrefixGroupToKey($item['key'], $groupName);
				if ($this->translateExists($item['key'])) {
					continue;
				}
				$translate = $this->repository->create($item);
				$this->addTranslate($translate->getAttribute('key'), $translate);
			}
		}
	}

	private function getTranslateByKey(string $key): ?Translate
	{
		return \Arr::get($this->translates, $key);
	}

	private function translateExists(string $key): bool
	{
		return (bool)$this->getTranslateByKey($key);
	}

	private function addPrefixGroupToKey(string $key, string $prefix)
	{
		return implode('.', [$prefix, $key]);
	}

	private function getBuilder(?string $key = null)
	{
		$builder = new class implements \ArrayAccess {
			private $key = '';

			private $value = '';

			private $type = 'text';

			private $displayName = '';

			private $subGroup = '';

			/** @var array */
			private $variables = [];

			public function setKey(string $key): self
			{
				$this->key = $key;
				return $this;
			}

			public function setTypeText(): self
			{
				$this->type = Translate::TYPE_TEXT;
				return $this;
			}

			public function setTypeTextarea(): self
			{
				$this->type = Translate::TYPE_TEXTAREA;
				return $this;
			}

			public function setTypeEditor(): self
			{
				$this->type = Translate::TYPE_EDITOR;
				return $this;
			}

			public function setValue(?string $value = null): self
			{
				$this->value = $value;
				return $this;
			}

			public function setDisplayName(string $displayName): self
			{
				$this->displayName = $displayName;
				return $this;
			}

			/**
			 * @param array $variables
			 */
			public function setVariables(array $variables): self
			{
				$this->variables = $variables;
				return $this;
			}

			public function build()
			{
				return [
					'key'          => $this->key,
					'value'        => $this->value,
					'type'         => $this->type,
					'display_name' => $this->displayName,
					'variables'    => $this->variables,
					'sub_group'    => $this->subGroup,
				];
			}

			public function offsetGet($offset)
			{
				return $this->offsetExists($offset) ? $this->{$offset} : null;
			}

			public function offsetExists($offset)
			{
				return property_exists($this, $offset);
			}

			public function offsetSet($offset, $value)
			{
				$method = Str::camel('set' . ucfirst($offset));
				if (method_exists($this, $method)) {
					$this->$method($value);
				}
			}

			public function offsetUnset($offset)
			{
				if ($this->offsetExists($offset)) {
					$this->{$offset} = null;
				}
			}

			/**
			 * @param string $subGroup
			 */
			public function setSubGroup(string $subGroup): self
			{
				$this->subGroup = $subGroup;
				return $this;
			}

		};
		if ($key) {
			$builder->setKey($key);
		}
		return $builder;
	}

	private function addTranslate(string $key, Translate $translate)
	{
		$this->translates->offsetSet($key, $translate);
	}
}
