<?php
	namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(LanguageTableSeeder::class);
		$this->call(AdminsTableSeeder::class);
		$this->call(RolesAndPermissionsSeeder::class);
        $this->call(AdminMenuSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(TranslateTableSeeder::class);
//        $this->call(PageSeeder::class);
        $this->call(MenuGroupSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(MetaTableSeeder::class);
        $this->call(RedirectsTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(ProductTableSeeder::class);
    }
}
