<?php
	namespace Database\Seeders;

	use App\Events\Admin\MenusChanged;
	use App\Models\Menu;
	use App\Models\MenuGroup;
	use App\Models\MenuLang;
	use Illuminate\Database\Seeder;

	class MenuTableSeeder extends Seeder
	{
		private $languages;

		public function __construct()
		{
			$this->languages = \App\Models\Language::all();
		}

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
			$menuGroups = [
				'main_menu' => [
				],
			];
			$menuGroups = $this->arrayReverseRecursive($menuGroups);
			$default = ['active' => 1];
			foreach ($menuGroups as $role => $menus) {
				if (!$menus) {
					continue;
				}
				$group = MenuGroup::getByRoleName($role);
				if (!$group) {
					continue;
				}
				foreach ($menus as $item) {
					$item = $this->arrayMerge($default, $item);
					$menu = $this->saveMenu($item, $group);
					if (Arr::has($item, 'childrens')) {
						foreach (Arr::get($item, 'childrens') as $itemChild) {
							$itemChild = $this->arrayMerge($default, $itemChild);
							$itemChild['parent_id'] = $menu->id;
							$this->saveMenu($itemChild, $group);
						}
					}
				}
			}


			event(new MenusChanged());
		}

		private function saveMenu(array $item, MenuGroup $menuGroup): Menu
		{
			($menu = new Menu())->fillExisting($item)
				->menuGroup()->associate($menuGroup)->save()
			;
			foreach ($this->languages as $language) {
				($menuLang = new MenuLang())->fillExisting($item);
				$menuLang->menu()->associate($menu);
				$menuLang->associateWithLanguage($language)
					->save()
				;
			}
			return $menu;
		}

		// for remove inspection warning - using merge in loop
		private function arrayMerge()
		{
			return array_merge(...func_get_args());
		}

		private function arrayReverseRecursive($arr)
		{
			foreach ($arr as $key => $val) {
				if (is_array($val)) {
					$arr[ $key ] = $this->arrayReverseRecursive($val);
				}
			}
			return array_reverse($arr);
		}
	}
