<?php declare(strict_types=1);

namespace Database\Seeders;
use App\Repositories\CategoryRepository;
use Faker\Generator;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;

class CategoryTableSeeder extends AbstractSeeder
{
	use WithFaker;

    /**
     * @var Generator
     */
    private $generator;
    /**
     * @var CategoryRepository
     */
    private $repository;

    /**
     *
     * @param CategoryRepository $repository
     * @param Generator $generator
     */

    public function __construct(CategoryRepository $repository, Generator $generator)
    {
        $this->setUpFaker();
        $this->generator = $generator;
        $this->repository = $repository;
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		if (!$this->isDevSeedEnabled()){
			return;
		}
        $categories = [
            [
                'name' => 'AVK',
                'image' => $this->image($this->generator->imageUrl()),

            ],
            [
                'name' => 'MILKA',
                'image' => $this->image($this->generator->imageUrl()),
            ],
            [
                'name' => 'KINDER',
                'image' => $this->image($this->generator->imageUrl()),
            ],
            [
                'name' => 'ROSHEN',
                'image' => $this->image($this->generator->imageUrl()),
            ],
        ];
        $this->loop($categories);
    }

    private function loop(array $categories)
    {
        foreach ($categories as $category) {
            $category['url'] = Str::slug($category['name']);
            $this->createCategories($category);
        }
    }

    private function createCategories(array $pageData): void
    {
        $this->repository->create($pageData);
    }
}
