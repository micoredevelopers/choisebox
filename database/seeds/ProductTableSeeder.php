<?php declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use Faker\Generator;
use Illuminate\Foundation\Testing\WithFaker;

class ProductTableSeeder extends AbstractSeeder
{
	use WithFaker;

	/**
	 * @var ProductRepository
	 */
	private $repository;
	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;

	public function __construct(ProductRepository $repository, CategoryRepository $categoryRepository)
	{
		$this->setUpFaker();
		$this->repository = $repository;
		$this->categoryRepository = $categoryRepository;
	}


	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		if (!$this->isDevSeedEnabled()){
			return;
		}
		/**    @var $category Category */
		foreach ($this->categoryRepository->all() as $category) {
			foreach (range(1, 10) as $item) {
				$data = [
					'name' => $this->faker->streetName,
					'description' => $this->faker->text(),
					'weight' => $this->faker->numberBetween(5, 50),
					'price' => $this->faker->numberBetween(5, 15),
					'pricePerKg' => $this->faker->numberBetween(90, 500),
					'pricePerOne' => $this->faker->numberBetween(1, 5),
					'image' => $this->image($this->faker->imageUrl()),
				];
				$this->repository->createRelated($data, $category);
			}
		}
	}

}
