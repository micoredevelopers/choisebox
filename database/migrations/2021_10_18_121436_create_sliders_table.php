<?php declare(strict_types=1);

use App\Builders\Migration\MigrationBuilder;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
    use MigrationCreateFieldTypes;

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'sliders';

   private $foreignKey = 'sliders_id';


   public function __construct()
   {
       $this->builder = app(MigrationBuilder::class);
   }


   public function up(): void
    {
        Schema::create($this->table, function (Blueprint $table) {
            $this->builder->setTable($table);

            $table->bigIncrements('id');
            $this->builder
                ->createImage()
                ->createSort()
                ->createActive()
            ;
            $table->timestamps();
        });

    }




    public function down(): void
    {
        Schema::dropIfExists($this->table);
    }
}
