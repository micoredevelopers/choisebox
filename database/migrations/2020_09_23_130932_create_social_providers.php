<?php

use App\Builders\Migration\MigrationBuilder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialProviders extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	private $table = 'social_providers';

	private $foreignKey = 'user_id';
	private $foreignTable = 'users';

	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}


	public function up()
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->builder->setTable($table);

			$table->id();
			$table->unsignedBigInteger('user_id')->index();
			$this->builder->addForeign($this->foreignKey, $this->foreignTable);
			$this->builder
				->createNullableString('provider_id')
				->createNullableChar('provider')
			;
			$table->timestamps();
		});
	}


	public function down()
	{
		Schema::dropIfExists($this->table);
	}
}
