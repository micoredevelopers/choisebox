<?php declare(strict_types=1);

use App\Builders\Migration\MigrationBuilder;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxesTable extends Migration
{

   /**
    * @var MigrationBuilder
   */
   private $builder;

   private $table = 'boxes';

   private $foreignKey = 'box_id';

   private $tableLang = 'box_langs';

    use MigrationCreateFieldTypes;



   public function up(): void
    {
        Schema::create('boxes' , function (Blueprint $table) {
            $this->setTable($table);

            $table->id('id');
            $this
                ->createImage()
                ->createFloatPrice()
            ;
            $this->createNullableChar('size');
            $this->createNullableChar('weight');

            $this->createUniqueUrl();
            $this->createActive();
            $this->createSort();

            $table->timestamps();
        });


        Schema::create($this->tableLang, function (Blueprint $table) {
            $this->setTable($table);
            $table->unsignedBigInteger($this->foreignKey);

            $this->createName()
                ->createLanguageKey()
            ;
            $table->foreign($this->foreignKey)
                ->references('id')->on($this->table)
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }


    public function down(): void
    {
        Schema::dropIfExists($this->tableLang);
        Schema::dropIfExists($this->table);
    }
}
