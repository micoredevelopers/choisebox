<?php declare(strict_types=1);

use App\Builders\Migration\MigrationBuilder;
use App\Traits\Migrations\ForeignKeys\CategoryForeignKey;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	private $table = 'products';

	private $foreignKey = 'products_id';

	private $tableLang = 'products_langs';

	use MigrationCreateFieldTypes;
	use CategoryForeignKey;

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function (Blueprint $table) {
			$this->setTable($table);

			$table->bigIncrements('id');
			$this
				->createImage()
				->createWeight()
				->createFloatPrice()
			;

			$this->addForeignCategory();
			$this->createActive();
			$this->createSort();

			$table->timestamps();

		});

		Schema::create('product_lang', function (Blueprint $table) {
			$this->setTable($table);

			$this->createName()
				->createDescription()
			;

			$this->createLanguageKey();
			$this->addForeignProduct();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('product_lang');

		Schema::dropIfExists('products');
	}

	private function addForeignProduct()
	{
		$this->table()->unsignedBigInteger('product_id')->index()->nullable();
		$this->table()->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
	}
}
