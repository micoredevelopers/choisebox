<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeletedByTable extends Migration
{
	use \App\Traits\Migrations\MigrationCreateFieldTypes;
	use \App\Traits\Migrations\ForeignKeys\UsersForeignKey;

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(): void
	{
		Schema::create('deleted_by', function (Blueprint $table) {
			$this->setTable($table);
			$table->bigIncrements('id');
			$table->nullableMorphs('deletable');
			$this->addForeignUsers();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(): void
	{
		Schema::dropIfExists('deleted_by');
	}
}
