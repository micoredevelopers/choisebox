<?php declare(strict_types=1);

use App\Builders\Migration\MigrationBuilder;
use App\Traits\Migrations\MigrationCreateFieldTypes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{

	use MigrationCreateFieldTypes;

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

	private $table = 'orders';

	private $foreignKey = 'orders_id';

	private $tableLang = 'orders_langs';

	public function __construct()
	{
		$this->builder = app(MigrationBuilder::class);
	}


	public function up(): void
	{
		Schema::create($this->table, function (Blueprint $table) {
			$this->setTable($table);
			$table->id();

			$this->createPrice()
				->createNullableChar('name')
				->createNullableChar('surname')
				->createNullableChar('phone')
				->createNullableChar('email')
				->createNullableChar('city')
				->createNullableChar('address')
				->createNullableChar('delivery_type')
				->createNullableChar('payment_type')
				->createNullableChar('pickup_point')
			;
            $table->string('comment', 500)->nullable();
            $table->decimal('amount', 10, 2)->unsigned();
			$table->unsignedSmallInteger('box_amount')->default(0);

            $table->timestamps();
        });
	}


	public function down(): void
	{
		Schema::dropIfExists($this->tableLang);
		Schema::dropIfExists($this->table);
	}
}
