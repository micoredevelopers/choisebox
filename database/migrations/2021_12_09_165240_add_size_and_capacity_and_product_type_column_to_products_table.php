<?php declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Builders\Migration\MigrationBuilder;

class AddSizeAndCapacityAndProductTypeColumnToProductsTable extends Migration
{

	/**
	 * @var MigrationBuilder
	 */
	private $builder;

    public function __construct()
    {
      $this->builder = app(MigrationBuilder::class);
    }


    public function up(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $this->builder->setTable($table);
            $this->builder->createNullableChar('product_type');
            $this->builder->createNullableChar('size');
            $this->builder->createNullableChar('capacity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('product_type');
            $table->dropColumn('size');
            $table->dropColumn('capacity');
        });
    }
}
