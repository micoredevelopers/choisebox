<?php declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\Product\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
	/**
	 * @var Product
	 */
	public $resource;

	public function toArray($request)
	{
		$data = array_merge(
			$this->resource->toArray(),
			$this->resource->lang ? $this->resource->lang->toArray() : []
		);

		$data['imagePath'] = getPathToImage($this->resource->getImage());

		return $data;
	}
}
