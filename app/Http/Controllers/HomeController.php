<?php declare(strict_types=1);

namespace App\Http\Controllers;

class HomeController extends SiteController
{
	public function home()
	{
		return view('public.pages.home.index');
	}

}
