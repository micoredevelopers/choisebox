<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Debug\LoggerHelper;
use App\Models\Translate\Translate;
use App\Repositories\TranslateRepository;
use App\Traits\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class TranslateController extends AdminController
{
	use Authorizable;

	private $name;
	protected $tb;

	protected $routeKey = 'translate';

	protected $permissionKey = 'translate';
	/**
	 * @var TranslateRepository
	 */
	private $translateRepository;

	public function __construct(TranslateRepository $translateRepository)
	{
		parent::__construct();
		$this->name = __('modules.localization.title');
		$this->tb = 'translate';
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->addScripts([
			'js/lib/select2/select2.full.min.js',
			'js/lib/select2/ru.js',
		]);
		$this->addCss([
			'css/lib/select2.min.css',
		]);
		$this->shareViewModuleData();
		$this->translateRepository = $translateRepository;
	}

	public function index(Request $request)
	{
		if ($request->has('seed')) {
			try {
				seedByClass('TranslateTableSeeder');
			} catch (\Throwable $e) {
				$this->setFailMessage($e->getMessage());
			}
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}
		$data = [];
		$list = $this->translateRepository->getForAdminDisplay($request);
		$groups = $list->pluckDistinct('group');
		$groups = collect($groups);
		$this->setTitle($this->name);
		$active = request()->session()->get('setting_tab', old('setting_tab', ($groups->first())));
		$with = compact(array_keys(get_defined_vars()));
		if (view()->exists('admin.translate.index')) {
			$data['content'] = view('admin.translate.index', $with);
		}

		return $this->main($data);
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		$vars['groups'] = Translate::getGroups();
		$this->addBreadCrumb(__('form.creating'));
		$data['content'] = view('admin.translate.add')->with($vars);
		return $this->main($data);
	}

	public function store(Request $request)
	{
		if ($translate = $this->translateRepository->create($request->all())) {
			$this->setSuccessStore();
		}
		return $this->redirectOnCreated($translate);
	}

	public function update(Request $request, $id)
	{
		if ('*' === $id) {
			return $this->bulkUpdate($request);
		}
		$translate = $this->translateRepository->find($id);
		// костыль
		$data = Arr::first(Arr::first($request->all()));
		$this->translateRepository->update($data, $translate);
		$this->setSuccessUpdate();
		return $this->redirectOnUpdated($translate);

	}

	public function bulkUpdate(Request $request)
	{
		if ($request->has('translate')) {
			$ids = array_keys($request->get('translate'));
			$this->setSuccessUpdate();
			$translates = Translate::with('lang')->find($ids);
			$translateRequest = $request->get('translate');
			/** @var  $translates Translate[] */
			foreach ($translates as $translate) {
				try {
					$data = Arr::get($translateRequest, $translate->getKey());
					$this->translateRepository->update($data, $translate);
					//touch for observer works
					if (!$translate->wasChanged() && $translate->lang->wasChanged()) {
						$translate->touch();
					}
				} catch (\Throwable $e) {
					app(LoggerHelper::class)->error($e);
				}
			}
		}
		request()->flashOnly('setting_tab');
		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

}
