<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PageRequest;
use App\Models\Admin\Photo;
use App\Models\Page\Page;
use App\Repositories\PageRepository;
use App\Services\Admin\Page\PageShowFieldsService;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Prettus\Validator\Exceptions\ValidatorException;

class PageController extends AdminController
{
	use SaveImageTrait;
	use Authorizable;

	private $name;

	protected $key = 'pages';

	protected $permissionKey = 'pages';

	protected $routeKey = 'admin.pages';
	/**
	 * @var PageRepository
	 */
	private $repository;

	public function __construct(PageRepository $repository)
	{
		parent::__construct();
		$this->name = __('modules.pages.title');
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->repository = $repository;
	}

	public function index(Request $request, PageRepository $pageRepository)
	{
		$this->setTitle($this->name);
		$vars['list'] = $pageRepository->getListAdmin($request);
		$vars['request'] = $request;

		$data['content'] = view('admin.pages.index', $vars);

		return $this->main($data);
	}

	/**
	 * @param PageRepository $pageRepository
	 * @return Factory|View
	 */
	public function create(PageRepository $pageRepository)
	{
		$pageTypes = Page::getPageTypes();
		$list = $pageRepository->getForAdmin();
		$data['content'] = view('admin.pages.create')->with(compact('pageTypes', 'list'));

		return $this->main($data);
	}

	/**
	 * @param PageRequest $request
	 * @return RedirectResponse|Redirector
	 * @throws ValidatorException
	 */
	public function store(PageRequest $request)
	{
		$input = $request->except('image');
		if ($page = $this->repository->create($input)) {
			$this->setSuccessStore();
			$this->saveImage($request, $page);
		}

		return $this->redirectOnCreated($page);
	}

	public function edit(Page $page)
	{
		$container = (new PageShowFieldsService($page))->generate();
		$title = $this->titleEdit($page->lang, 'title');
		$this->addBreadCrumb($title)->setTitle($title);
		view()->share([
			'edit' => $page,
			'photosList' => $page->images,
			'container' => $container,
		]);

		$data['content'] = $this->getViewByPage($page);

		return $this->main($data);
	}

	private function getViewByPage(Page $page)
	{
		return view('admin.pages.edit');
	}

	/**
	 * @param PageRequest $request
	 * @param Page $page
	 * @return RedirectResponse|Redirector
	 * @throws ValidatorException
	 */
	public function update(PageRequest $request, Page $page)
	{
		$input = $request->except('image', 'sub_image');
		if ($this->repository->update($input, $page)) {
			$this->setSuccessUpdate();
		}

		$container = (new PageShowFieldsService($page))->generate();
		if ($container->isWithImage()) {
			$this->saveImage($request, $page);
			$this->saveImage($request, $page, 'sub_image');
		}

		return $this->redirectOnUpdated($page);
	}

	/**
	 * @param Page $page
	 * @param Photo $photo
	 * @return RedirectResponse|Redirector
	 * @throws \Exception
	 */
	public function destroy(Page $page, Photo $photo)
	{
		if ($page->delete()) {
			$photo->deleteImageStorage($page->image);
			$photo->deleteImageStorage($page->getAttribute('sub_image'));
			$this->setSuccessDestroy();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

}
