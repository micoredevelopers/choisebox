<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\Product;

use App\DataContainers\Admin\Products\ProductsSearchDataContainer;
use App\DataContainers\Product\ProductSearchData;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests\Admin\Product\ProductRequest;
use App\Models\Admin\Photo;
use App\Models\Category\Category;
use App\Models\Product\Product;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use App\Traits\Models\ImageAttributeTrait;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;


class ProductController extends AdminController
{
	use Authorizable;

	use SaveImageTrait;

	use ImageAttributeTrait;

	protected $routeKey = 'admin.products';

//	protected $permissionKey = 'products';

	protected $key = 'products';

	protected $name = 'Товары';

	/**
	 * @var ProductRepository
	 */
	private $productRepository;
	private $languagesList;
	/**
	 * @var CategoryRepository
	 */
	private $categoryRepository;


	public function __construct(
		ProductRepository $productRepository,
		CategoryRepository $categoryRepository
	)
	{
		parent::__construct();
		$this->productRepository = $productRepository;
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->categoryRepository = $categoryRepository;
	}

	public function index(ProductSearchData $searchContainer, Request $request)
	{
		if ($request->get('category')) {
			$searchContainer->setCategory($this->categoryRepository->find($request->get('category')));
		}
		$categories = $this->categoryRepository->all();
		$this->setTitle($this->name);
		$products = $this->productRepository->findPaginated($searchContainer);
		$products->appends(request()->input());
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('admin.products.index', $with);

		return $this->main($data);
	}

	public function create()
	{
		$vars['categories'] = Category::all();
		$data['content'] = view('admin.products.create')->with($vars);

		return $this->main($data);
	}

	public function store(ProductRequest $request)
	{
		if ($product = $this->productRepository->create($request->only($request->getFillableFields()))) {
			$this->setSuccessStore();
		}

		if ($request->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $product->getKey()))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}


	public function edit($id)
	{
		$vars['edit'] = $product = $this->productRepository->findForEdit((int)$id);

		if (!$product) {
			$this->setMessage(__('modules._.record-not-finded'));

			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}
		$title = $this->titleEdit($product);
		$this->addBreadCrumb($title)->setTitle($title);
		$vars['categories'] = Category::all();
		$data['content'] = view('admin.products.edit', $vars)->with($vars);

		return $this->main($data);
	}

	public function update(ProductRequest $request, Product $product)
	{
		if ($this->productRepository->update($request->only($request->getFillableFields()), $product)) {
			$this->setSuccessUpdate();
			$this->saveImage($request, $product);
			$this->saveImage($request, $product, 'icon');
		}

		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());

	}

	/**
	 * @param Category $product
	 * @param Photo $photo
	 * @return RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	public function destroy(Product $product, Photo $photo)
	{
		if ($product->delete()) {
			$photo->deleteImageStorage($product->getImage());
			$this->setSuccessDestroy();
			$this->fireEvents();
		}

		return back()->with($this->getResponseMessage());
	}


	private function fireEvents()
	{

	}


}
