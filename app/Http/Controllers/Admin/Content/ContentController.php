<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Admin\AdminController;
use App\Repositories\ContentRepository;
use App\Traits\Controllers\SaveImageTrait;

class ContentController extends AdminController
{
	use SaveImageTrait;

	protected $name = '';

	protected $contentType = '';

	protected $repository;

	protected $fields = [];

	public function __construct(ContentRepository $repository)
	{
		parent::__construct();
		$this->addBreadCrumb($this->name, $this->resourceRoute('index'));
		$this->shareViewModuleData();
		$this->repository = $repository;
		view()->share('fields', arrayValueAsKey(array_flip($this->fields)));
	}

	public function index()
	{
		$this->setTitle($this->name);
		$list = $this->repository->getListPublicByType($this->contentType);
		$list->load('lang');
		$with = compact(array_keys(get_defined_vars()));
		$data['content'] = view('admin.how.index', $with);

		return $this->main($data);
	}

	public function create()
	{
		$data['content'] = view('admin.how.create');
		return $this->main($data);
	}

	public function store(CategoryRequest $request)
	{
		$input = $request->only($request->getFillableFields());
		$input['type'] = $this->contentType;
		if ($how = $this->repository->create($input)) {
			$this->saveImage($request, $how);
			$this->setSuccessStore();
		}

		if ($request->has('createOpen')) {
			return redirect($this->resourceRoute('edit', $how->getKey()))->with($this->getResponseMessage());
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

	public function edit($howId)
	{
		$how = $this->repository->findTyped($howId, $this->contentType);
		$vars['edit'] = $how;
		$title = $this->titleEdit($how);
		$this->addBreadCrumb($title)->setTitle($title);
		$data['content'] = view('admin.how.edit', $vars);

		return $this->main($data);
	}

	public function update(CategoryRequest $request, $howId)
	{
		$how = $this->repository->findTyped($howId, $this->contentType);
		$input = $request->only($request->getFillableFields());
		if ($this->repository->update($input, $how)) {
			$this->saveImage($request, $how);
			$this->setSuccessUpdate();
		}
		if ($request->has('saveClose')) {
			return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
		}

		return redirect()->back()->with($this->getResponseMessage());
	}


	public function destroy($howId)
	{
		$how = $this->repository->findTyped($howId, $this->contentType);
		if ($this->repository->delete($how)) {
			$this->setSuccessDestroy();
		}

		return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
	}

}
