<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\Content;

use App\Repositories\ContentRepository;
use App\Traits\Authorizable;

class ReviewsController extends ContentController
{
	use Authorizable;

	protected $fields = ['name', 'title'];

	protected $routeKey = 'admin.';

	protected $name = 'Отзывы';

	public function __construct(ContentRepository $repository)
	{
		$this->routeKey .= $this->permissionKey = $this->contentType = 'reviews';
		parent::__construct($repository);
		config()->set('contents.thumbnail.width', 172);
		config()->set('contents.thumbnail.height', 172);
	}


}
