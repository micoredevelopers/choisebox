<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin\Category;

use App\Events\Admin\CategoriesChanged;
use App\Helpers\Media\ImageRemover;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Requests\Admin\Category\CategoryRequest;
use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use App\Traits\Authorizable;
use App\Traits\Controllers\SaveImageTrait;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;


class CategoryController extends AdminController
{
    use Authorizable;

    use SaveImageTrait;

    protected $routeKey = 'admin.categories';

    protected $key = 'categories';

    protected $name = 'Категории';

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct();
        $this->categoryRepository = $categoryRepository;
        $this->addBreadCrumb($this->name, $this->resourceRoute('index'));

        $this->shareViewModuleData();

    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $this->setTitle($this->name);
        $list = $this->categoryRepository->getListAdmin();
        $data['content'] = view('admin.category.index', compact('list'));
        return $this->main($data);
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $vars['categories'] = $this->categoryRepository->getListAdmin();
        $data['content'] = view('admin.category.create')->with($vars);

        return $this->main($data);
    }

    public function store(CategoryRequest $request)
    {
        if ($category = $this->categoryRepository->create($request->only($request->getFillableFields()))) {
            $this->setSuccessStore();
            event(new CategoriesChanged());
        }
        $this->saveImage($request, $category);
        $this->saveImage($request, $category, 'icon');

        return $this->redirectOnCreated($category);
    }

    public function edit(Category $category)
    {
        $vars['edit'] = $category;
        if (!$category) {
            $this->setMessage(__('modules._.record-not-finded'));
            return redirect($this->resourceRoute('index'))->with($this->getResponseMessage());
        }

        $this->addBreadCrumb($category->getNameDisplay(), $this->resourceRoute('index', $category->getKey()));

        $title = $this->titleEdit($category);
        $this->addBreadCrumb($title)->setTitle($title);

        $data['content'] = view('admin.category.edit', $vars)->with($vars);

        return $this->main($data);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        if ($this->categoryRepository->update($request->only($request->getFillableFields()), $category)) {
            $this->setSuccessUpdate();
            $this->saveImage($request, $category);
            $this->saveImage($request, $category, 'icon');

            event(new CategoriesChanged());
        }

        return $this->redirectOnUpdated($category);
    }

    public function destroy(Category $category, ImageRemover $imageRemover)
    {
        $category->delete();
        $imageRemover->removeImage($category->image);
        $this->setSuccessDestroy();
        event(new CategoriesChanged());


        return back()->with($this->getResponseMessage());
    }


}