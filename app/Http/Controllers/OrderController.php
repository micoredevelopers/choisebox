<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Mail\OrderMailer;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Services\Cart\CartCalcService;
use App\Services\Cart\UserCartService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OrderController extends SiteController
{

	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	/**
	 * @var UserCartService
	 */
	private $cartService;
	/**
	 * @var CartCalcService
	 */
	private $calc;

	public function __construct(
		OrderRepository $orderRepository,
		UserCartService $cartService,
		CartCalcService $calc
	)
	{
		parent::__construct();
		$this->orderRepository = $orderRepository;
		$this->cartService = $cartService;
		$this->calc = $calc;
	}

	/**
	 * Сообщение об успешном оформлении заказа
	 * @param Request $request
	 * @return Application|Factory|View|RedirectResponse
	 */
	public function success(Request $request)
	{
		if (!$request->session()->exists('order_id')) {
			return redirect()->route('category.show');
		}
		// сюда покупатель попадает сразу после успешного оформления заказа
		$order_id = $request->session()->pull('order_id');
		$order = Order::findOrFail($order_id);

		return view('public.pages.cart.includes.thx', compact('order'));

	}

	public function create(OrderRequest $request)
	{
		$cart = $this->cartService->getCart();
		if (null === $cart) {
			abort(404);
		}
		$cartCost = $this->calc->getDataCalc()->getCartCost();

		try {
			\DB::beginTransaction();
			$order = $this->orderRepository->create($request->validated() + ['amount' => $cartCost]);
			$cartData = $this->cartService->getCartData($this->calc->getDataCalc());

			foreach ($cartData->getProducts() as $cartProduct) {
				$cartProduct = $order->items()->create([
					$cartProduct->getProduct()->getForeignKey() => $cartProduct->getProduct()->getKey(),
					'name' => $cartProduct->getProduct()->getProductName(),
					'price' => $cartProduct->getProduct()->getPrice(),
					'quantity' => $cartProduct->getQuantity(),
					'cost' => $cartProduct->getProduct()->getPrice() * $cartProduct->getQuantity(),
				]);
				$order->getOrderProducts()->push($cartProduct);
			}
			try{
				Mail::queue((app(OrderMailer::class, compact('order'))));
				Mail::queue((app(OrderMailer::class, compact('order'))->to($order->getEmail())));
			} catch (\Throwable $e){
			    logger()->error($e);
			}

			$cart->delete();
			\DB::commit();
		} catch (\Throwable $e) {
			\DB::rollBack();
			$this->setFailMessage('Ошибка при оформлении заказа');
			return redirect(back()->with($this->getResponseMessage()));
		}

		return redirect()
			->route('cart.success')
			->with('order_id', $order->getKey())
		;
	}

}
