<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\DataContainers\Product\ProductSearchData;
use App\Enum\ProductSortEnums;
use App\Enum\ProductTypeEnum;
use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;

class CategoryController extends SiteController
{
	/**
	 * @var ProductRepository
	 */
	private $productRepository;

    public function __construct(ProductRepository $productRepository)
	{
		parent::__construct();
		$this->productRepository = $productRepository;
    }

	public function show(ProductSearchData $data, ?Category $category = null)
	{
		if ($category) {
			$data->setCategory($category);
		}
		$products = $this->productRepository->findPaginated($data);
		$products->appends(request()->input());
		$sortEnums = ProductSortEnums::getEnums();
        $typeEnums = ProductTypeEnum::getEnums();

		$with = compact(array_keys(get_defined_vars()));

		return view('public.pages.products.index')->with($with);
	}
}
