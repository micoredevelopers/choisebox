<?php

namespace App\Http\Controllers;

use App\Enum\ProductTypeEnum;
use App\Helpers\Media\ImageSaver;
use App\Models\Category\Category;
use App\Models\Product\Product;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use function PHPUnit\Framework\fileExists;


class XLSXParserController extends SiteController
{
    /**
     * @var ImageSaver
     */
    private $imageSaver;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(
        ImageSaver $imageSaver,
        CategoryRepository $categoryRepository,
        ProductRepository $productRepository
    )
    {
        parent::__construct();

        $this->imageSaver = $imageSaver;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
    }

    public function show()
    {
        if (!isLocalEnv()) {
            abort(403);
        }
        $files = [
            'files/import/Конти фіналка.xlsx' => 'Конти',
            'files/import/Бисквит шоколад  фіналка.xlsx' => 'Бисквит шоколад',
            'files/import/Kinder Фіналка.xlsx' => 'Kinder',
            'files/import/Milka без фігурок (фіналка).xlsx' => 'Milka',
            'files/import/ЖАКО  фіналка .xlsx' => 'Жако',
            'files/import/Nestle  фіналка .xlsx' => 'Nestle',
            'files/import/рошен финалка.xlsx' => 'Roshen',
            'files/import/SWEET Waffles  фіналка.xlsx' => 'SWEET Waffles',
            'files/import/АВК финалка.xlsx' => 'АВК',
            'files/import/Корона фіналка.xlsx' => 'Корона',
            'files/import/Лукас фіналка.xlsx' => 'Лукас',
            'files/import/Лісова казка  Фіналка.xlsx' => 'Лісова казка',
            'files/import/ЭЛИТНЫЕ  фіналка.xlsx' => 'ЭЛИТНЫЕ',
            'files/import/Світоч фіналка.xlsx' => 'Світоч',
            'files/import/readybox.xlsx' => 'Готовые подарки',

        ];
        $products = [];
        foreach ($files as $file => $categoryName) {
//			echo "<table>";
//			dump($file);

            $category = $this->categoryRepository->firstByNameOrCreate($categoryName);
            if (!fileExists($file)) {
                \Debugbar::info($file);
                continue;
            }
            $spreadsheet = IOFactory::load(public_path($file));
            $worksheet = $spreadsheet->getActiveSheet();
            $worksheetArray = $worksheet->toArray();
            unset($worksheetArray[0]);
            $worksheetArray = array_values($worksheetArray);
            $drawings = $worksheet->getDrawingCollection()->getArrayCopy();
            foreach ($worksheetArray as $key => $value) {
                if (null === $value[0]) {
                    continue;
                }
                [$name, $description, $image, $weight, $price] = $value;
                $image = $this->saveImage($drawings[$key], $name);
                if ($this->productRepository->isNameExists((string)$name)) {
                    continue;
                }
                $products[] = $this->productRepository->createRelated([
                    'name' => $name,
                    'description' => $description,
                    'image' => $image,
                    'weight' => (float)$weight,
                    'price' => (float)$price,
                ], $category);
            }
//			echo "</table>";
//			break;
        }
        dump(count($products));
    }

    private function saveImage(Drawing $drawing, $name): string
    {
//		[$startColumn, $startRow] = Coordinate::coordinateFromString($drawing->getCoordinates());
        $coo = $drawing->getCoordinates();
//		echo "<tr><td>$coo</td><td>$name</td>";
        $zipReader = fopen($drawing->getPath(), 'r+');

        $imageContents = '';
        while (!feof($zipReader)) {
            $imageContents .= fread($zipReader, 1024);
        }
        fclose($zipReader);

        $image = $this->imageSaver->withFileName(md5(base64_encode($imageContents)))
            ->withFileExtension($drawing->getExtension())
            ->setWithThumbnail(false)
            ->saveFromBase64(base64_encode($imageContents));
//		echo sprintf("<td><img width='100' src='%s'><br></td></tr>", getPathToImage($image));
        return $image;
    }

}


