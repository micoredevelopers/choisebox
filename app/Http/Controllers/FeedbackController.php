<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Mail\FeedbackMailer;
use App\Models\Feedback\Feedback;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends SiteController
{

    public function save(FeedbackRequest $request)
    {
        $trial = new Feedback();

        $trial->name = $request->name;
        $trial->phone = $request->phone;

        $trial->save();
		$data = [
			'name' => $trial->name,
			'phone' => $trial->phone,
		];
		$mail = app(FeedbackMailer::class, ['data' => $data]);
		Mail::queue($mail);

        if ($request->expectsJson()){
            return $this->setSuccessMessage('Success send')->getResponseMessageForJson();
        }
        return back()->with($this->getMessage());
    }
}
