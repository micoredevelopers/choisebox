<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enum\OrderDeliveryTypeEnum;
use App\Enum\OrderPaymentTypeEnum;
use App\Enum\OrderPickupPointTypeEnum;
use App\Http\Resources\ProductResource;
use App\Models\Cart;
use App\Models\Product\Product;
use App\Services\Cart\CartCalcService;
use App\Services\Cart\UserCartService;
use Illuminate\Http\Request;

class CartController extends SiteController
{
    /**
     * @var Cart
     */
    private $cart;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var UserCartService
     */
    private $userCartService;
    /**
     * @var CartCalcService
     */
    private $calcService;

    public function __construct(Request $request, UserCartService $userCartService, CartCalcService $calcService)
    {
        parent::__construct();
        $this->request = $request;
        $this->userCartService = $userCartService;
        $this->calcService = $calcService;
    }

    public function index()
    {
        $products = $this->userCartService->getOrCreate()->getProducts();
        $deliveryTypes = OrderDeliveryTypeEnum::getEnums();
        $paymentTypes = OrderPaymentTypeEnum::getEnums();
        $pickupTypes = OrderPickupPointTypeEnum::getEnums();
        $with = compact(array_keys(get_defined_vars()));

        return view('public.pages.cart.index')->with($with);
    }

    public function add(Request $request)
    {
        $quantity = (int)($request->input('quantity') ?? 1);
        $product = Product::findOrFail($request['id']);
        $this->userCartService->getOrCreate()->increase($product->getKey(), $quantity);

        $this->setResponseData(['product' => ProductResource::make($product)]);
        $this->addCartDataToResponse();
        return $this->response();
    }

    public function changeAmount($id, Request $request)
    {
        $this->getCart()->change($id, $request['count']);
        $this->addCartDataToResponse();

        return $this->response();
    }

    public function changeAmountInc($id)
    {
        $this->getCart()->increase($id);
        $this->addCartDataToResponse();

        return $this->response();
    }

    public function changeAmountDec($id)
    {
        $this->getCart()->decrease($id);
        $this->addCartDataToResponse();

        return $this->response();
    }

    /**
     * Удаляет товар с идентификаторм $id из корзины
     */
    public function remove($id)
    {
        $this->getCart()->remove($id);
        $this->addCartDataToResponse();

        // выполняем редирект обратно на страницу корзины
        return $this->response();
    }

    /**
     * Полностью очищает содержимое корзины покупателя
     */
    public function clear()
    {
        $this->getCart()->delete();
        $this->addCartDataToResponse();

        // выполняем редирект обратно на страницу корзины
        return $this->response();
    }

    private function getCart()
    {
        if (null === $this->cart) {
            $this->cart = $this->userCartService->getCart();
        }

        return $this->cart;
    }

    private function response()
    {
        return $this->request->expectsJson() ? $this->getResponseMessageForJson() : redirect(route('cart.index'));
    }

    private function addCartDataToResponse()
    {
        $this->setResponseData($this->calcService->getDataCalc()->toArray());
    }
}
