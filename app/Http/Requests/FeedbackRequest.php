<?php declare(strict_types=1);

namespace App\Http\Requests;


class FeedbackRequest extends AbstractRequest
{
	public function rules()
	{
		return [
			'name' => $this->getRuleRequiredChar(),
			'phone' => $this->getBasePhoneRule(),
		];

	}

	protected function mergeRequestValues()
	{
		$this->mergeFormatPhone();
	}
}
