<?php declare(strict_types=1);

namespace App\Http\Requests;

use App\Contracts\Requests\RequestParameterModelable;
use App\Enum\OrderDeliveryTypeEnum;
use App\Enum\OrderPaymentTypeEnum;
use App\Enum\OrderPickupPointTypeEnum;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Traits\Requests\Helpers\GetActionModel;
use Illuminate\Validation\Rule;

class OrderRequest extends AbstractRequest implements RequestParameterModelable
{
	use GetActionModel;

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */

	public function rules()
	{
		$rules = [
			'name' => 'required|max:255',
			'surname' => 'required|max:255',
			'email' => 'required|email|max:255',
			'delivery_type' => ['required', Rule::in(OrderDeliveryTypeEnum::$enums)],
			'payment_type' => ['required', Rule::in(OrderPaymentTypeEnum::$enums)],
			'pickup_point' => ['required', Rule::in(OrderPickupPointTypeEnum::$enums)],
			'city' => 'max:255',
			'address' => 'max:255',
			'branch' => 'max:255',
			'phone' => 'required|max:16',
			'comment' => ['max:500'],
			'box_amount' => ['min:1', 'max:' . ValidationMaxLengthHelper::MEDIUMINT_UNSIGNED],
		];

		return $rules;
	}

	protected function mergeRequestValues()
	{
		$this->mergeFormatPhone();
	}

}
