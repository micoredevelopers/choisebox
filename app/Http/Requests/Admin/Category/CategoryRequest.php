<?php

namespace App\Http\Requests\Admin\Category;

use App\Contracts\Requests\RequestParameterModelable;
use App\Http\Requests\AbstractRequest;
use App\Models\Category\Category;
use App\Traits\Requests\Helpers\GetActionModel;

class CategoryRequest extends AbstractRequest implements RequestParameterModelable
{

    protected $toBooleans = ['active'];

    use GetActionModel;

    protected $requestKey = 'category';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required', 'max:255'],
            'url' => ['required', 'max:255'],
            'image' => ['required', 'image'],
            'product_type' => ['max:255'],
        ];

        if ($this->isMethod('PATCH')) {
            return [
                'name' => ['required', 'max:255'],
                'url' => ['required', 'max:255'],
                'product_type' => ['required', 'max:255'],
            ];
        }

        return $rules;
    }

    protected function mergeRequestValues()
    {
        $this->mergeUrlFromName();
        if ($this->has('parent_id')) {
            $this->merge([
                'parent_id' => (int)$this->get('parent_id'),
            ]);
        }
    }
}
