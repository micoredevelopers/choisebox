<?php

namespace App\Http\Requests\Admin\Product;

use App\Contracts\Requests\RequestParameterModelable;
use App\Helpers\Validation\ValidationMaxLengthHelper;
use App\Http\Requests\AbstractRequest;
use App\Models\Category\Category;
use App\Traits\Requests\Helpers\GetActionModel;

class ProductRequest extends AbstractRequest implements RequestParameterModelable
{

    protected $toBooleans = ['active'];

    use GetActionModel;

    protected $requestKey = 'product';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'weight' => ['max:255'],
            'price' => ['required', 'max:255'],
            'image' => ['image'],
            'name' => ['required', 'max:255'],
            'size' => ['max:255'],
            'capacity' => ['max:255'],
            'product_type' => ['max:255'],
            'category_id' => ['required', 'exists:categories,id'],
            'description.*' => ['max:' . ValidationMaxLengthHelper::TEXT],
        ];

//        if ($this->isActionUpdate() AND $category = $this->getActionModel()) {
//            /** @var  $category Category */
//            $rules['url'] = ['required', 'unique:products,url,' . $category->id];
//        }

        return $rules;
    }

    protected function mergeRequestValues()
    {
        $this->mergeUrlFromName();
        $this->merge([
        ]);
    }
}
