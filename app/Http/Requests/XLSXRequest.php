<?php declare(strict_types=1);

namespace App\Http\Requests;

use App\Http\Requests\AbstractRequest;

class XLSXRequest extends AbstractRequest
{
    public function rules()
        {

                $rules = [
                    'file_name' => ['required', 'max:255'],
                ];
                return $rules;

        }
}
