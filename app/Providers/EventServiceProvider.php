<?php declare(strict_types=1);

namespace App\Providers;

use App\Events\Admin\MenusChanged;
use App\Events\Admin\NewsChangedEvent;
use App\Events\Admin\NewsCreatedEvent;
use App\Events\Cabinet\PaymentCallbackEvent;
use App\Events\Cabinet\PerformerCertificationUploadedEvent;
use App\Events\Chat\MessageSubmitted;
use App\Events\Feedback\FeedbackCreated;
use App\Events\Order\BeforeOrderClosedEvent;
use App\Events\Order\Chat\ChatCreated;
use App\Events\Order\Chat\UsersAddedToChat;
use App\Events\Order\OrderBeforePublished;
use App\Events\Order\OrderBeforeUpdatedEvent;
use App\Events\Order\OrderClosedEvent;
use App\Events\Order\OrderCreatedEvent;
use App\Events\Order\OrderPublished;
use App\Events\Order\OrderUpdatedEvent;
use App\Events\Order\Reviews\AddCustomerReviewEvent;
use App\Events\Order\Reviews\AddPerformerReviewEvent;
use App\Events\Order\Reviews\BeforeAddCustomerReviewEvent;
use App\Events\Order\Reviews\BeforeAddPerformerReviewEvent;
use App\Events\Order\Show\Bids\BeforeChoosePerformer;
use App\Events\Order\Show\Bids\BidBeforeDisabledEvent;
use App\Events\Order\Show\Bids\BidBeforeEditedEvent;
use App\Events\Order\Show\Bids\BidCanceledEvent;
use App\Events\Order\Show\Bids\BidCreatedEvent;
use App\Events\Order\Show\Bids\BidDeletedEvent;
use App\Events\Order\Show\Bids\BidEditedEvent;
use App\Events\Order\Show\Bids\ChoosedPerformer;
use App\Events\Order\Show\OrderViewedEvent;
use App\Events\Platform\Cabinet\UserCertificationUpdatedEvent;
use App\Events\Platform\User\Analytics\UserLastSeedUpdated;
use App\Listeners\Admin\Menu\DropMenuCache;
use App\Listeners\Admin\News\NewsRebuildNextPrevListener;
use App\Listeners\Admin\NewsCreatedNotifyUsersListener;
use App\Listeners\Admin\User\OnUserAuth;
use App\Listeners\Cabinet\PaymentCallbackAddPremiumListener;
use App\Listeners\Cabinet\PerformerCertificationNotifyAdminListener;
use App\Listeners\Chat\MessageSubmittedNotifyUnread;
use App\Listeners\Feedback\NotifyAdminFeedbackListener;
use App\Listeners\Order\RecalcBidsListener;
use App\Listeners\Order\Review\CalculateAVGRatingReviewListener;
use App\Listeners\Order\Review\CheckToNeedsPublishReviews;
use App\Listeners\Order\Review\NotifyPerformerAboutCustomerComment;
use App\Listeners\Order\Review\UserRatingChangedListener;
use App\Listeners\Order\Show\Bids\BidCreatedListener;
use App\Listeners\Order\Show\Bids\BidDecrementLimitEditsListener;
use App\Listeners\Order\Show\MakeChatWorksOnChoosePerformerListener;
use App\Listeners\Order\Show\NotifyPerformerListener;
use App\Listeners\Order\Show\OnOrderViewedIncrementOrderViewsListener;
use App\Listeners\Order\Update\DecrementLimitEditsListener;
use App\Listeners\Platform\Cabinet\UserCertificationPerformerCategoryListener;
use App\Listeners\Platform\User\Analytics\UserLastSeedUpdatedListener;
use App\Listeners\User\Registration\UserRegisteredFirstStepListener;
use App\Listeners\View\Compose\AdminLanguagesListener;
use App\Listeners\View\Compose\MainViewListener;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package App\Providers
 * @see OrderWasMakedListener
 */
class EventServiceProvider extends ServiceProvider
{
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		Registered::class => [],
		Login::class => [
			OnUserAuth::class,
		],
		MenusChanged::class => [
			DropMenuCache::class,
		],
		\App\Events\Admin\Image\ImageUploaded::class => [
//            \App\Listeners\Admin\Image\ImageUploadedListener::class,
		],
		\App\Events\Admin\Image\MultipleImageUploaded::class => [
//            \App\Listeners\Admin\Image\MultipleImageUploadedListener::class,
		],
		'creating: admin.langlist' => [
			AdminLanguagesListener::class
		],
		'creating: *' => [
            MainViewListener::class
		]
	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
		parent::boot();
	}
}
