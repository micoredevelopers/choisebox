<?php declare(strict_types=1);

namespace App\Listeners\View\Compose;

use App\Models\Language;
use Illuminate\View\View;

class AdminLanguagesListener
{
	public function handle(View $view)
	{
		if (!$this->supports()) {
			return;
		}
		$view->with(['languages' => Language::all()]);
	}

	private function supports(): bool
	{
		if (app()->runningInConsole()) {
			return false;
		}

		return true;
	}
}
