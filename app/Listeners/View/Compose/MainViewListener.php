<?php declare(strict_types=1);

namespace App\Listeners\View\Compose;

use App\DataContainers\Cart\CartData;
use App\DataContainers\Cart\CartProductData;
use App\Enum\ProductTypeEnum;
use App\Models\Cart;
use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use App\Services\Cart\CartCalcService;
use App\Services\Cart\UserCartService;

class MainViewListener
{
    /**
     * @var UserCartService
     */
    private $userCartService;
    /**
     * @var CartCalcService
     */
    private $cartCalcService;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var CartProductData
     */
    private $cartProductData;
    /**
     * @var CartData
     */
    private $cartData;


    /**
     * MainViewListener constructor.
     * @param UserCartService $userCartService
     * @param CartCalcService $cartCalcService
     * @param CartData $cartData
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(
        UserCartService $userCartService,
        CartCalcService $cartCalcService,
        CartData $cartData,
        CategoryRepository $categoryRepository
    )
    {
        $this->userCartService = $userCartService;
        $this->cartCalcService = $cartCalcService;
        $this->categoryRepository = $categoryRepository;
        $this->cartData = $cartData;
    }

    private static $isLoaded = false;

    public function handle($event)
    {

        if (!$this->supports()) {
            return;
        }
        self::$isLoaded = true;
        $categories = $this->categoryRepository->getListPublic();

        $cartCalcData = $this->cartCalcService->getDataCalc();
        $cartData = $this->userCartService->getCartData($cartCalcData);

        $with = compact(array_keys(get_defined_vars()));
        \view()->share($with);
    }

    private function supports(): bool
    {
        if (app()->runningInConsole()) {
            return false;
        }
        if (self::$isLoaded) {
            return false;
        }

        return true;
    }
}
