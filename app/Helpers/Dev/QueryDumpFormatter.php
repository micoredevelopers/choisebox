<?php


namespace App\Helpers\Dev;


use Illuminate\Support\Str;

class QueryDumpFormatter
{

	public static function fillValuesFromLog($query)
	{
		$res = [];
		foreach ($query as $q) {
			$res[] = self::fillValues($q['query'], (array)$q['bindings']);
		}
		return $res;
	}

	public static function fillValues($q, array $values)
	{
		foreach ($values as $value) {
			if (is_bool($value)) {
				$value = (int)$value;
			}
			$q = Str::replaceFirst('?', $value, $q);
		}
		return $q;

	}

	public static function format($q, array $values = [])
	{

		$q = self::fillValues($q, $values);

		$matches = [
			'left join',
			'inner join',
		];
		foreach ($matches as $match) {
			if (!Str::contains($q, $match)) {
				continue;
			}
			$parts = explode($match, $q);
			$q = implode(PHP_EOL . $match, $parts);

		}
		return $q;
	}

	public static function fromQueryLog($query)
	{
		$res = [];
		foreach ($query as $q) {
			$res[] = self::format($q['query'], (array)$q['bindings']);
		}
		return $res;
	}
}