<?php

namespace App\Rules\Feedback;

use App\Helpers\Debug\LoggerHelper;
use Illuminate\Contracts\Validation\Rule;

class UploadLimitRule implements Rule
{
	public $limit = 50;

	/**
	 * Create a new rule instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Determine if the validation rule passes.
	 *
	 * @param string $attribute
	 * @param mixed $value
	 * @return bool
	 */
	public function passes($attribute, $value)
	{
		if (!$value) {
			return true;
		}
		$files = array_filter((array)$value, function ($item){
			try{
				return is_file($item);
			} catch (\Throwable $e){
			    return false;
			}
		});
		$total_size = array_reduce($files, function ($sum, $item) {
			// each item is UploadedFile Object
			try{
				$sum += filesize($item->path());
			} catch (\Throwable $e){
			    app(LoggerHelper::class)->error($e);
			}
			return $sum;
		});
		//in MB
		$total_size /= (1024 * 1024);

		return $total_size <= $this->limit;
	}

	/**
	 * Get the validation error message.
	 *
	 * @return string
	 */
	public function message()
	{
		return getTranslate('feedback.validation.files-total-size.fail', 'Общий размер загружаемых файлов не должен превышать 50 Мб');
	}

	/**
	 * @param int $limit
	 * @return UploadLimitRule
	 */
	public function setLimit(int $limit): UploadLimitRule
	{
		$this->limit = $limit;
		return $this;
	}
}
