<?php declare(strict_types=1);

namespace App\DataContainers\Cart;

use App\Models\Product\Product;

/**
 * Class CartProductData
 * @package App\DataContainers\Cart
 */
final class CartProductData
{
	/**
	 * @var Product
	 */
	private $product;

	private $quantity = 0;

	/**
	 * @return Product
     */
	public function getProduct(): Product
    {
		return $this->product;
	}

	/**
	 * @return int
	 */
	public function getQuantity(): int
	{
		return $this->quantity;
	}

	public static function createFromCartProduct(Product $product): self
	{
	    $self = new self();
	    $self->product = $product;
		$self->quantity = (int)($product->pivot->quantity ?? 0);

	    return $self;
	}


}