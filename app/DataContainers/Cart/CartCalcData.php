<?php

namespace App\DataContainers\Cart;

class CartCalcData
{
	private $cartCost;

	private $cartWeight;

	private $totalProducts = 0;

	/**
	 * @return float
	 */
	public function getCartCost(): float
	{
		return floatFormatted($this->cartCost);
	}

	/**
	 * @param mixed $cartCost
	 * @return self
	 */
	public function setCartCost(float $cartCost): self
	{
		$this->cartCost = floatFormatted($cartCost);

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCartWeight(): float
	{
		return floatFormatted($this->cartWeight);
	}

	/**
	 * @param mixed $cartWeight
	 * @return self
	 */
	public function setCartWeight(float $cartWeight): self
	{
		$this->cartWeight = floatFormatted($cartWeight);

		return $this;
	}

	public function toArray(): array
	{
		return get_object_vars($this);
	}

	/**
	 * @return int
	 */
	public function getTotalProducts(): int
	{
		return $this->totalProducts;
	}

	/**
	 * @param int $totalProducts
	 * @return CartCalcData
	 */
	public function setTotalProducts(int $totalProducts): CartCalcData
	{
		$this->totalProducts = $totalProducts;

		return $this;
	}


}