<?php declare(strict_types=1);

namespace App\DataContainers\Cart;

use App\Enum\ProductTypeEnum;
use App\Models\Category\Category;
use App\Models\Product\Product;

final class CartData
{
	/**
	 * @var int
	 */
	private $cartId;

	/**
	 * @var CartCalcData
	 */
	private $calcData;

	/**
	 * @var array<CartProductData>
	 */
	private $products = [];

	/**
	 * @return CartCalcData
	 */
	public function getCalcData(): CartCalcData
	{
		return $this->calcData;
	}

	/**
	 * @param CartCalcData $calcData
	 * @return CartData
	 */
	public function setCalcData(CartCalcData $calcData): CartData
	{
		$this->calcData = $calcData;

		return $this;
	}

	/**
	 * @return CartProductData[]
	 */
	public function getProducts(): array
	{
		return $this->products;
	}

	/**
	 * @param CartProductData $product
	 * @return CartData
	 */
	public function addProduct(CartProductData $product): CartData
	{
		$this->products[$product->getProduct()->getKey()] = $product;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getCartId(): int
	{
		return $this->cartId;
	}

	/**
	 * @param int $cartId
	 * @return CartData
	 */
	public function setCartId(int $cartId): CartData
	{
		$this->cartId = $cartId;

		return $this;
	}

	public function getProductCartDataByProduct(Product $product): ?CartProductData
	{
		return $this->products[$product->getKey()] ?? null;
	}

	public function isProductInCart(Product $product): bool
	{
		return null !== $this->getProductCartDataByProduct($product);
	}

    public function getProductQuantity(Product $product)
    {
        return $this->getProductCartDataByProduct($product)->getQuantity();
    }

    public function getProductType(Product $product): ProductTypeEnum
    {
        $type = $this->getProductCartDataByProduct($product)->getProductCategory()->find($product->category()->getParentKey(),'product_type');

        return new ProductTypeEnum($type);
    }


}