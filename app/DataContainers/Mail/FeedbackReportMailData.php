<?php declare(strict_types=1);

namespace App\DataContainers\Mail;

use App\Models\Feedback\Feedback;

final class FeedbackReportMailData
{
	/**
	 * @var string
	 */
	private $name;

    /**
     * @var string
     */
    private $phone;

    public static function createFromEntity(Feedback $feedback): self
	{
		$self = new self();
		$self->name = (string)$feedback->getAttribute('name');
        $self->phone = (string)$feedback->getAttribute('phone');



        return $self;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

    public function getPhone(): string
    {
        return $this->phone;
    }



}