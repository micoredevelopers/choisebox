<?php declare(strict_types=1);

namespace App\DataContainers\Product;

use App\Enum\ProductSortEnums;
use App\Models\Category\Category;
use Illuminate\Http\Request;

final class ProductSearchData
{

	private $sort = 'id';

	private $sortDirection = 'DESC';
	/**
	 * @var Category|null
	 */
	private $category;

	private $search = '';

	private $limit = 15;

	public function __construct(Request $request)
	{
		if ($request->has('sort') && in_array($request->get('sort'), ProductSortEnums::$enums, true)) {
			[$sortField, $sortDirection] = explode('.', $request->get('sort'));
			$this->sort = $sortField;
			$this->sortDirection = $sortDirection;
		}
		if ($request->has('search')) {
			$this->search = $request->get('search');
		}
	}

	/**
	 * @return Category|null
	 */
	public function getCategory(): ?Category
	{
		return $this->category;
	}

	public function getCategoryId(): ?int
	{
		return $this->category ? $this->category->getKey() : null;
	}

	/**
	 * @param Category|null $category
	 * @return ProductSearchData
	 */
	public function setCategory(?Category $category): self
	{
		$this->category = $category;

		return $this;
	}

	/**
	 * @return mixed|string
	 */
	public function getSort()
	{
		return $this->sort;
	}

	/**
	 * @return mixed|string
	 */
	public function getSortDirection()
	{
		return $this->sortDirection;
	}

	/**
	 * @return string
	 */
	public function getSearch(): ?string
	{
		return $this->search;
	}

	/**
	 * @return int
	 */
	public function getLimit(): int
	{
		return $this->limit;
	}


}