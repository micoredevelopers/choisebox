<?php declare(strict_types=1);

namespace App\Models\Slider;

use App\Models\Model;


/**
 * App\Models\Slider\SliderItem
 *
 * @property int $id
 * @property int $slider_id
 * @property bool $active
 * @property int|null $sort
 * @property string|null $link
 * @property string $type
 * @property string|null $src
 * @property-read \App\Models\Slider\Slider $slider
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereSliderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereSrc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slider\SliderItem whereType($value)
 * @mixin \Eloquent
 */
class SliderItem extends Model
{

    const TYPE_IMAGE = 'image';


    protected $guarded = [
        'id',
    ];
    protected $casts = [
        'active' => 'boolean',
    ];

    public $timestamps = false;

    public function slider()
    {
        return $this->belongsTo(Slider::class);
    }

    public function isTypeImage()
    {
        return $this->type === self::TYPE_IMAGE;
    }

    public static function boot()
    {
        parent::boot();

        if (class_exists(SliderItemObserver::class)) {
            SliderItem::observe(SliderItemObserver::class);
        }
    }
}
