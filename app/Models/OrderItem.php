<?php declare(strict_types=1);

namespace App\Models;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\OrderItem
 *
 * @property int $id
 * @property int $order_id
 * @property int|null $product_id
 * @property string $name
 * @property string $price
 * @property int $quantity
 * @property string $cost
 * @property-read Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereQuantity($value)
 * @mixin \Eloquent
 */
class OrderItem extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'product_id',
		'name',
		'price',
		'quantity',
		'cost',
	];

	/**
	 * Связь «элемент принадлежит» таблицы `order_items` с таблицей `products`
	 */
	public function product(): BelongsTo
	{
		return $this->belongsTo(Product::class);
	}

	public function getProduct(): Product
	{
		return $this->product;
	}

	public function getProductId()
	{
		return $this->product()->getParentKey();
	}

	public function getName(): string
	{
		return (string)$this->getAttribute('name');
	}

	public function getPrice(): float
	{
		return (float)$this->getAttribute('price');
	}

	public function getQuantity(): int
	{
		return (int)$this->getAttribute('quantity');
	}

	public function getCost(): float
	{
		return (float)$this->getAttribute('cost');
	}

}
