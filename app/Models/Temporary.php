<?php

namespace App\Models;

/**
 * App\Models\Temporary
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $type
 * @property int $deletable
 * @property array|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary query()
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary whereDeletable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Temporary whereValue($value)
 * @mixin \Eloquent
 */
class Temporary extends Model
{
	protected $table = 'temporary';
	protected $fillable = ['key', 'value'];

	protected $guarded = ['id'];

	protected $casts = [
		'value' => 'array',
	];

	public function getData(): array
	{
		return (array)$this->value;
	}

	public function setData(array $data): Temporary
	{
		$this->setAttribute('value', $data);
		return $this;
	}
}
