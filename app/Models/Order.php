<?php declare(strict_types=1);

namespace App\Models;

use App\Enum\OrderDeliveryTypeEnum;
use App\Enum\OrderPaymentTypeEnum;
use App\Enum\OrderPickupPointTypeEnum;
use Illuminate\Support\Collection;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property float $price
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $city
 * @property string|null $address
 * @property string|null $delivery_type
 * @property string|null $payment_type
 * @property string|null $pickup_point
 * @property string|null $comment
 * @property string $amount
 * @property int $box_amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderItem[] $items
 * @property-read int|null $items_count
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereBoxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeliveryType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePaymentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePickupPoint($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
	protected $guarded = ['id'];
	protected $casts = [
		'pickup_point'
	];

	protected $fillable = [
		'name',
		'surname',
		'phone',
		'email',
		'delivery_type',
		'city',
		'address',
		'delivery_type',
		'payment_type',
		'box_amount',
		'comment',
		'amount',
		'pickup_point',
	];

	/**
	 * Связь «один ко многим» таблицы `orders` с таблицей `order_items`
	 */
	public function items()
	{
		return $this->hasMany(OrderItem::class);
	}

	/**
	 * @return Collection|array<OrderItem>
	 */
	public function getOrderProducts(): Collection
	{
		return $this->items;
	}

	public function getOrderDeliveryTypeEnum(): OrderDeliveryTypeEnum
	{
		return new OrderDeliveryTypeEnum((string)$this->getAttribute('delivery_type'));
	}

	public function getOrderPaymentTypeEnum(): OrderPaymentTypeEnum
	{
		return new OrderPaymentTypeEnum((string)$this->getAttribute('payment_type'));
	}

	public function getPickupTypeEnum(): OrderPickupPointTypeEnum
	{
		return new OrderPickupPointTypeEnum((int)$this->getAttribute('pickup_point'));
	}

	public function getName(): string
	{
		return (string)$this->getAttribute('name');
	}

	public function getSurname(): string
	{
		return (string)$this->getAttribute('surname');
	}

	public function getEmail(): string
	{
		return (string)$this->getAttribute('email');
	}

	public function getAddress(): string
	{
		return (string)$this->getAttribute('address');
	}

	public function getBranch(): string
	{
		return (string)$this->getAttribute('branch');
	}

	public function getPhone(): string
	{
		return (string)$this->getAttribute('phone');
	}

	public function getComment(): string
	{
		return (string)$this->getAttribute('comment');
	}

	public function getCity(): string
	{
		return (string)$this->getAttribute('city');
	}

	public function getBoxAmount():int
	{
		return (int)$this->getAttribute('box_amount');
	}

}
