<?php

namespace App\Models\Category;

use App\Models\ModelLang;


/**
 * App\Models\Category\CategoryLang
 *
 * @property int $category_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $description
 * @property-read \App\Models\Category\Category $category
 * @property-read \App\Models\Language $language
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryLang whereName($value)
 * @mixin \Eloquent
 */
class CategoryLang extends ModelLang
{

	protected $primaryKey = ['category_id', 'language_id'];

	protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
	}

}
