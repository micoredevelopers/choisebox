<?php declare(strict_types=1);

namespace App\Models\Category;

use App\Contracts\HasLocalized;
use App\Enum\ProductTypeEnum;
use App\Models\Model;
use App\Models\Product\Product;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\Localization\RedirectLangColumn;


/**
 * App\Models\Category\Category
 *
 * @property int $id
 * @property string|null $image
 * @property int|null $sort
 * @property string|null $url
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category\CategoryLang[] $langs
 * @property-read int|null $langs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUrl($value)
 * @mixin \Eloquent
 */
class Category extends Model implements HasLocalized
{
	use RedirectLangColumn;
	use ImageAttributeTrait;


	protected $langColumns = ['name', 'language_id', 'description'];

	protected $hasOneLangArguments = [CategoryLang::class];

	protected $guarded = ['id'];


	public function getCategoryName()
	{
		return $this->getNameDisplay();
	}

	public function getNameDisplay(): ?string
	{
		return (string)$this->getAttribute('name');
	}

	public function getPrimaryValue()
	{
		return $this->getAttribute('id');
	}

	public function products()
	{
		return $this->hasMany(Product::class);
	}

	public function getSlug(): string
	{
		return (string)$this->getAttribute('url');
	}

    public function getProductType()
    {
        return $this->getAttribute('product_type');
    }

    public function getProductTypeEnum(): ProductTypeEnum
    {
        $type = $this->getAttribute('product_type') ?: ProductTypeEnum::CANDIES;

        return new ProductTypeEnum($type);
    }


}
