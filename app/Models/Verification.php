<?php

namespace App\Models;

use App\Observers\VerificationObserver;
use App\Platform\Auth\Services\Register\VerificationAvailableService;
use Illuminate\Support\Carbon;

/**
 * App\Models\Verification
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Verification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Verification query()
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @mixin \Eloquent
 */
class Verification extends Model
{
	protected $guarded = ['id'];

	protected $dates = ['available_at'];

	public function getPhone(): string
	{
		return (string)$this->getAttribute('phone');
	}

	public function getCode(): string
	{
		return (string)$this->getAttribute('code');
	}

	public function setCode($code): self
	{
		$this->setAttribute('code', $code);
		return $this;
	}

	public function getAvailableAt(): Carbon
	{
		return getDateCarbon($this->getAttribute('available_at'));
	}

	public function isAvailable()
	{
		return $this->getAvailableAt() <= now();
	}

	public function getDiffInSeconds()
	{
		return $this->getAvailableAt()->diffInSeconds(now());
	}

	public function setIsVerified(): Verification
	{
		$this->setAttribute('verified_at', now());
		return $this;
	}

	public function isVerified(): bool
	{
		return null !== $this->getAttribute('verified_at');
	}

	protected static function boot()
	{
		parent::boot();
		if (class_exists(VerificationObserver::class)) {
			static::observe(VerificationObserver::class);
		}
	}

	public function getAvailablityChecker()
	{
		return app(VerificationAvailableService::class, ['verification' => $this]);
	}


}
