<?php

namespace App\Models\Product;

use App\Models\ModelLang;
use App\Traits\EloquentMultipleForeignKeyUpdate;
use App\Traits\Models\BelongsToLanguage;


/**
 * App\Models\Category\ProductLang
 *
 * @property string|null $name
 * @property string|null $sub_name
 * @property string|null $description
 * @property string|null $except
 * @property int $language_id
 * @property int|null $product_id
 * @property-read \App\Models\Language $language
 * @property-read Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model default()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model getLang($languageId = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model parentMenu($parentId = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model sortOrder($sort = 'asc', $id = 'desc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductLang whereExcept($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereId($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIdIn($ids, $field = 'id')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereIsPublished($column = 'published_at')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductLang whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Product\ProductLang whereSubName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Model whereUrl($url)
 * @mixin \Eloquent
 */
class ProductLang extends ModelLang
{
	use BelongsToLanguage;

	use EloquentMultipleForeignKeyUpdate;

	protected $table = 'product_lang';

	protected $primaryKey = ['product_id', 'language_id'];

	protected $guarded = [];

	public function product()
	{
		return $this->belongsTo(Product::class);
	}
}
