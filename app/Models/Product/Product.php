<?php declare(strict_types=1);

namespace App\Models\Product;

use App\Contracts\HasLocalized;
use App\Enum\ProductTypeEnum;
use App\Models\Category\Category;
use App\Models\Model;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\Localization\RedirectLangColumn;


/**
 * App\Models\Product\Product
 *
 * @property int $id
 * @property string|null $image
 * @property float|null $weight
 * @property float $price
 * @property int|null $category_id
 * @property int $active
 * @property int|null $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product\ProductLang[] $langs
 * @property-read int|null $langs_count
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereWeight($value)
 * @mixin \Eloquent
 */
class Product extends Model implements HasLocalized
{
    use RedirectLangColumn;

    use ImageAttributeTrait;

    protected $langColumns = ['name', 'language_id', 'description',];

    protected $hasOneLangArguments = [ProductLang::class];

    protected $guarded = ['id'];

    protected $langAdmin = ['id'];

    public function getProductName(): ?string
    {
        return $this->getNameDisplay();
    }

    public function getNameDisplay(): ?string
    {
        return (string)$this->getAttribute('name');
    }

    public function getDescription()
    {
        return $this->getAttribute('description');
    }

    public function getWeight()
    {
        return $this->getAttribute('weight');
    }

    public function getPrice(): float
    {
        return (float)$this->getAttribute('price');
    }

    public function getCapacity()
    {
        return $this->getAttribute('capacity');
    }

    public function getSize()
    {
        return $this->getAttribute('size');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function getProductTypeEnum(): ProductTypeEnum
    {
        $type = $this->category->getProductType();
        if ($type == null)
            return new ProductTypeEnum('default');
        return new ProductTypeEnum($type);
    }


    public function setProductType(int $type)
    {
        return $this->setAttribute('product_type', $type);
    }


}
