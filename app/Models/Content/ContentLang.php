<?php

	namespace App\Models\Content;

	use App\Models\ModelLang;
	use Illuminate\Database\Eloquent\Relations\BelongsTo;

	/**
 * App\Models\Content\ContentLang
 *
 * @property int $content_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $title
 * @property string|null $excerpt
 * @property string|null $description
 * @property-read \App\Models\Content\Content $content
 * @property-read \App\Models\Language $language
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|ContentLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ContentLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContentLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|ContentLang whereContentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContentLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContentLang whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|ContentLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContentLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ContentLang whereTitle($value)
 * @mixin \Eloquent
 */
class ContentLang extends ModelLang
	{

		protected $fillable = [
			'content_id',
			'language_id',
			'name',
			'title',
			'except',
			'description',
		];

		protected $primaryKey = ['content_id', 'language_id'];

		public function content(): BelongsTo
		{
			return $this->belongsTo(Content::class);
		}
	}
