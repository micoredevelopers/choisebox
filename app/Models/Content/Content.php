<?php

	namespace App\Models\Content;


	use App\Contracts\HasLocalized;
	use App\Models\Model;
	use App\Traits\Models\ImageAttributeTrait;
	use App\Traits\Models\Localization\RedirectLangColumn;
	use Illuminate\Database\Eloquent\Relations\MorphTo;

	/**
 * App\Models\Content\Content
 *
 * @property int $id
 * @property string|null $contentable_type
 * @property int|null $contentable_id
 * @property string|null $type
 * @property string|null $content_type
 * @property string|null $url
 * @property string|null $image
 * @property int|null $sort
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $contentable
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Content\ContentLang[] $langs
 * @property-read int|null $langs_count
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Content newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Content newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content query()
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereContentableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereContentableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Content whereUrl($value)
 * @mixin \Eloquent
 */
class Content extends Model implements HasLocalized
	{
		use RedirectLangColumn;
		use ImageAttributeTrait;

		protected $langColumns = ['name', 'title', 'excerpt', 'description', 'language_id'];

		protected $guarded = ['id'];

		protected $fillable = [
			'contentable_type',
			'contentable_id',
			'type',
			'image',
			'sort',
			'active',
		];

		protected $hasOneLangArguments = [ContentLang::class];

		public function contentable(): MorphTo
		{
			return $this->morphTo();
		}

	}
