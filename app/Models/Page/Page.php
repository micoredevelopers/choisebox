<?php

namespace App\Models\Page;


use App\Contracts\HasLocalized;
use App\Enum\PageTypeEnum;
use App\Models\Content\Content;
use App\Models\Content\HasContentable;
use App\Models\Model;
use App\Traits\Models\ImageAttributeTrait;
use App\Traits\Models\Localization\RedirectLangColumn;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Arr;
use InvalidArgumentException;
use TypeError;


/**
 * App\Models\Page\Page
 *
 * @property int $id
 * @property int|null $sort
 * @property string|null $url
 * @property int $active
 * @property string|null $image
 * @property string|null $sub_image
 * @property string|null $page_type
 * @property string|null $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Content[] $content
 * @property-read int|null $content_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Page\PageLang[] $langs
 * @property-read int|null $langs_count
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page wherePageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereSubImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUrl($value)
 * @mixin \Eloquent
 */
class Page extends Model implements HasLocalized, HasContentable
{
	use ImageAttributeTrait;
	use RedirectLangColumn;

	protected $langColumns = [
		'name', 'title', 'description', 'excerpt', 'sub_title', 'sub_description',
	];

	protected $hasOneLangArguments = [PageLang::class];

	protected $guarded = [
	];

	public function getUrl()
	{
		$column = 'url';

		return Arr::get($this->attributes, $column);
	}

	public function content(): MorphMany
	{
		return $this->morphMany(Content::class, 'contentable');
	}

	public function getTitle()
	{
		return $this->title ?: $this->name;
	}

	public function getTypeEnum(): ?PageTypeEnum
	{
		try {
			return new PageTypeEnum($this->page_type);
		} catch (InvalidArgumentException | TypeError $e) {
			return null;
		}
	}

}
