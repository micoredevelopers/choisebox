<?php

	namespace App\Models\Page;


	use App\Models\ModelLang;

	/**
 * App\Models\Page\PageLang
 *
 * @property int $page_id
 * @property int $language_id
 * @property string|null $name
 * @property string|null $title
 * @property string|null $description
 * @property string|null $excerpt
 * @property string|null $sub_title
 * @property string|null $sub_description
 * @property-read \App\Models\Language $language
 * @property-read \App\Models\Page\Page $page
 * @method static \Illuminate\Database\Eloquent\Builder|Model active($active = 1)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orWhereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLanguage($languageId)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereLike($column, $value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereSubTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageLang whereTitle($value)
 * @mixin \Eloquent
 */
class PageLang extends ModelLang
	{

		protected $primaryKey = ['page_id', 'language_id'];

		public function page()
		{
			return $this->belongsTo(Page::class);
		}

	}
