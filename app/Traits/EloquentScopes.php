<?php
/**
 * Created by PhpStorm.
 * User: aljajazva
 * Date: 2019-05-22
 * Time: 13:34
 */

namespace App\Traits;


trait EloquentScopes
{

	public function scopeActive($query, $active = 1)
	{
		return $query->where('active', '=', $active);
	}

	public function scopeWhereLanguage($query, $languageId)
	{
		return $query->where('language_id', '=', $languageId);
	}

	public function scopeWhereLike(\Illuminate\Database\Eloquent\Builder $query, $column, $value)
	{
		return $query->where($column, 'like', '%'.$value.'%');
	}

	public function scopeOrWhereLike($query, $column, $value)
	{
		return $query->orWhere($column, 'like', '%'.$value.'%');
	}

}
