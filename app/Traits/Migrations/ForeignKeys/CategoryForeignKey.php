<?php

namespace App\Traits\Migrations\ForeignKeys;

use Illuminate\Database\Schema\Blueprint;

trait CategoryForeignKey
{
    protected function addForeignCategory(): void
    {
        /** @var  $table Blueprint*/
        $table = $this->table();
        $table->unsignedBigInteger('category_id')->index()->nullable();
        $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
    }
}