<?php

namespace App\Traits\Models;

trait ImageAttributeTrait
{
	public function setImage($name)
	{
		if (is_string($name)) {
			$column = 'image';
			$this->attributes[$column] = $name;
		}
		return $this;
	}

	public function getImage(): string
	{
		$column = 'image';
		return (string)$this->getAttribute($column);
	}

}