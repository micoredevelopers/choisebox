<?php

	namespace App\Observers;

	use App\Models\Translate\Translate;

	class TranslateObserver
	{
		/**
		 * Handle the translate "created" event.
		 *
		 * @param \App\Models\Translate $translate
		 * @return void
		 */
		public function creating(Translate $translate)
		{

		}



		public function created(Translate $translate)
		{
			//
		}

		public function updating(Translate $translate)
		{
		}

		/**
		 * Handle the translate "updated" event.
		 *
		 * @param \App\Models\Translate $translate
		 * @return void
		 */
		public function updated(Translate $translate)
		{
		}

		/**
		 * Handle the translate "restored" event.
		 *
		 * @param \App\Models\Translate $translate
		 * @return void
		 */
		public function restored(Translate $translate)
		{
			//
		}


	}
