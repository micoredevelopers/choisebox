<?php declare(strict_types=1);

namespace App\Services\Cart;

use App\DataContainers\Cart\CartCalcData;
use App\Models\Cart;

class CartCalcService
{
	/**
	 * @var UserCartService
	 */
	private $cartService;

	public function __construct(UserCartService $cartService)
	{
		$this->cartService = $cartService;
	}

	public function getDataCalc(): CartCalcData
	{
		$cartCalcData = new CartCalcData;
		$products = $this->getCart()->getProducts();

		$cartWeight = 0;
		$cartCost = 0;
		$totalProducts = 0;
		foreach ($products as $item) {
			$totalProducts += $itemQuantity = $item->pivot->quantity;
			$cartCost += $item->getPrice() * $itemQuantity;
			if ($item->getProductTypeEnum()->isBox()){
				continue;
			}
			$cartWeight += $itemQuantity * $item->weight;
		}

		$cartCalcData->setCartCost((float)$cartCost);
		$cartCalcData->setCartWeight((float)$cartWeight);
		$cartCalcData->setTotalProducts($totalProducts);

		return $cartCalcData;
	}

	private function getCart():Cart
	{
		return $this->cartService->getCart() ?? new Cart();
	}

}