<?php

namespace App\Services\Cart;

use App\DataContainers\Cart\CartCalcData;
use App\DataContainers\Cart\CartData;
use App\DataContainers\Cart\CartProductData;
use App\Models\Cart;
use App\Repositories\CartRepository;

class UserCartService
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    private $cart = false;

	public function getCart(): ?Cart
	{
	    //todo make guarantee return cart
		if ($this->cart instanceof Cart) {
			return $this->cart;
		}
		if (!$cartId = session('cart_id')) {
			return null;
		}
		$this->cart = Cart::find($cartId);

		return $this->cart;
	}

	public function getOrCreate(): Cart
	{
		$cart = $this->getCart() ?? Cart::create();
		session(['cart_id' => $cart->getKey()]);

		return $cart;
	}

	public function getCartData(CartCalcData $cartCalcData): CartData
	{
		$cart = $this->getCart() ?? new Cart();
		$cartData = (new CartData())
			->setCartId($cart->getKey())
			->setCalcData($cartCalcData);

        foreach ($this->cartRepository->getCartProducts($cart) as $cartProduct) {
            $cartData->addProduct(CartProductData::createFromCartProduct($cartProduct));
        }
		return $cartData;
	}
}