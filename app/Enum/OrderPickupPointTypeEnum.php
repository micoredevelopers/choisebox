<?php declare(strict_types=1);

namespace App\Enum;

class OrderPickupPointTypeEnum extends AbstractIntEnum
{
	public const SARATA = 1;

	public static $enums = [
		'пгт Сарата, Белгород-Днестровский район, Одесская область, Чкалова 50/б' => self::SARATA,
	];
}