<?php

namespace App\Enum;

class ProductTypeEnum extends AbstractStrEnum
{
	public const BOXES = 'box';
	public const CANDIES = 'candies';
	public const SET = 'set';
    public const DEFAULT = 'default';

	public static $enums = [
		'упаковок' => self::BOXES,
		'конфет' => self::CANDIES,
		'наборов' => self::SET,
        'товаров' => self::DEFAULT,
	];

	public function isBox(): bool
	{
		return $this->isEq(self::BOXES);
	}

	public function isCandy(): bool
	{
		return $this->isEq(self::CANDIES);
	}

	public function isSet(): bool
	{
		return $this->isEq(self::SET);
	}

}