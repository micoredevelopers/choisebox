<?php declare(strict_types=1);

namespace App\Enum;

final class PageTypeEnum extends AbstractStrEnum
{
	public const ABOUT = 'about';
	public const MAIN = 'main';
	public const OFFER = 'offer';
	public const PRIVACY = 'privacy';
	public const FOR_PERFORMER = 'for-performer';
	public const PAYMENT_AND_RETURN = 'payment-and-return';

	public static $enums = [
		'О нас' => self::ABOUT,
		'Главная' => self::MAIN,
		'Оферта' => self::OFFER,
		'Политика конфиденциальности' => self::PRIVACY,
		'Оплата и возврат' => self::PAYMENT_AND_RETURN,
	];

	public function isAbout(): bool
	{
		return $this->getKey() === self::ABOUT;
	}

	public function isMain(): bool
	{
		return $this->getKey() === self::MAIN;
	}



}