<?php

namespace App\Enum;

class OrderPaymentTypeEnum extends AbstractStrEnum
{
    public const CASH = 'COD';
    public const TRANSFER = 'transfer';
    public const CASHLESS = 'cashless';

    public static $enums = [
        'Наложным платежом' => self::CASH,
        'Переводом на карту' => self::TRANSFER,
        'Безналичным расчётом' => self::CASHLESS,
    ];

    public function isCash() :bool
    {
        return $this->isEq(self::CASH);
    }
    public function isTransfer() :bool
    {
        return $this->isEq(self::TRANSFER);
    }
    public function isCashless() :bool
    {
        return $this->isEq(self::CASHLESS);
    }

}