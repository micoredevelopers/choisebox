<?php declare(strict_types=1);

namespace App\Enum;

final class ProductSortEnums extends AbstractStrEnum
{
	public static $enums = [
		'по умолчанию' => '',
		'от дешевых к дорогим' => 'price.asc',
		'от дорогих к дешевым' => 'price.desc',
	];

}