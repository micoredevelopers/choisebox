<?php

namespace App\Enum;

class OrderDeliveryTypeEnum extends AbstractStrEnum
{
    public const NOVAPOSHTA = 'nmail';
    public const DELIVERY = 'delivery';
    public const PERSONAL_TRANSPORT = 'car';
    public const PICKUP = 'pickup';

    public static $enums = [
        'Новая почта' => self::NOVAPOSHTA,
        'Delivery' => self::DELIVERY,
        'Доставка транспортом компании' => self::PERSONAL_TRANSPORT,
        'Самовывозом' => self::PICKUP,
    ];

    public function isNovaposhta() :bool
    {
        return $this->isEq(self::NOVAPOSHTA);
    }

    public function isDelivery() :bool
    {
        return $this->isEq(self::DELIVERY);
    }

    public function isPersonalTransport() :bool
    {
        return $this->isEq(self::PERSONAL_TRANSPORT);
    }
    public function isPickUp() :bool
    {
        return $this->isEq(self::PICKUP);
    }

}