<?php

namespace App\Repositories;

use App\DataContainers\Product\ProductSearchData;
use App\Models\Product\Product;
use App\Models\Product\ProductLang;
use App\Traits\Repositories\RebuildNextAndPrev;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductRepository extends AbstractRepository
{
	use RebuildNextAndPrev;

	private $cacheKey = 'public.categories.tree';


	public function model()
	{
		return Product::class;
	}

	public function modelLang()
	{
		return ProductLang::class;
	}

	/**
	 * @param int $id
	 * @return Product|Builder|Builder[]|Collection|Model|null
	 */
	public function findForEdit(int $id)
	{
		return Product::with(['lang', 'category.lang'])->find($id);
	}

	public function isNameExists(string $name): bool
	{
		return Product::whereHas('langs', function ($q) use ($name) {
			return $q->where('name', $name);
		})
			->exists()
		;
	}

	public function findPaginated(ProductSearchData $data): LengthAwarePaginator
	{
		return Product::with('lang')
			->when($data->getSearch(), function (Builder $q) use ($data) {
				return $q->whereHas('langs', function (Builder $q) use ($data) {
					return $q->whereLike('name', $data->getSearch())
						->orWhereLike('description', $data->getSearch());
				});
			})
			->when($data->getCategory(), function (Builder $q) use ($data) {
				return $q->where($data->getCategory()->getForeignKey(), $data->getCategory()->getKey());
			})
			->orderBy($data->getSort(), $data->getSortDirection())
			->paginate($data->getLimit())
		;

	}
}
