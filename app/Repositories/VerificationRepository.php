<?php

	namespace App\Repositories;

	use App\Models\Order\Order;
	use App\Models\Verification;


	class VerificationRepository extends AbstractRepository
	{
		/**
		 * @return string
		 *  Return the model
		 */
		public function model()
		{
			return Verification::class;
		}

		public function findByOrder(Order $order): ?Verification
		{
			return $this->findByPhone($order->getPhone());
		}

		public function findByPhone($phone): ?Verification
		{
			return $this->findOneByField('phone', $phone);
		}

	}
