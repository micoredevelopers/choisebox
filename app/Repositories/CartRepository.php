<?php


namespace App\Repositories;


use App\Models\Cart;
use App\Models\Product\Product;
use Illuminate\Support\Collection;

class CartRepository
{
    public function model()
    {
        return Cart::class;
    }

    /**
     * @param Cart $cart
     * @return Collection|Product[]
     */
    public function getCartProducts(Cart $cart):Collection
    {
        $cart->load('products.category');
        return $cart->getProducts()->sortBy(function (Product $product, $q) {
            return  $product->getCategory()->getProductTypeEnum()->isBox();
        });

    }

}