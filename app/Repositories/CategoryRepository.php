<?php declare(strict_types=1);

namespace App\Repositories;

/**
 * @method Category|null findByUrl(string $url)
 */

use App\Criteria\SortCriteria;
use App\DataContainers\Admin\Feedback\SearchDataContainer;
use App\Enum\ProductTypeEnum;
use App\Models\Category\Category;
use App\Models\Category\CategoryLang;
use Cache;
use Illuminate\Support\Str;

class CategoryRepository extends AbstractRepository
{
	private $cacheKey = 'public.categories.tree';

	public function model()
	{
		return Category::class;
	}

	public function modelLang()
	{
		return CategoryLang::class;
	}


	public function addAdminCriteriaToQuery()
	{
		$this->pushCriteria($this->app->make(SortCriteria::class));
	}

	public function addPublicCriteriaToQuery()
	{
		$this->pushCriteria($this->app->make(SortCriteria::class));
	}

	public function getListAdmin(?SearchDataContainer $searchDataContainer = null)
	{
		$this->addAdminCriteriaToQuery();
		$this->with('lang');
		return $this->get();
	}

	public function getListPublic(?SearchDataContainer $searchDataContainer = null)
	{
		$this->addPublicCriteriaToQuery();

		$this->with('lang');
		return $this->get();
	}


	public function dropCacheTree(): void
	{
		Cache::forget($this->cacheKey);
	}


	public function findForNestable($id)
	{
		static $categoriesNestable;
		if ($categoriesNestable === null) {
			$categoriesNestable = $this->all()->keyBy('id');
		}

		return \Arr::get($categoriesNestable, $id, []);
	}

	public function nestable(array $categories, $parent_id = 0)
	{
		/** @var $findCategory Category */
		foreach ($categories as $num => $category) {
			if ($findCategory = $this->findForNestable(\Arr::get($category, 'id'))) {
				$data = [
					'sort' => $num,
					'parent_id' => (int)$parent_id,
				];
				$findCategory->fill($data)->save();
				if (isset($category['children'])) {
					$this->nestable($category['children'], $category['id']);
				}
			}
		}
	}

	public function firstByNameOrCreate(string $name): Category
	{
		$category = Category::whereHas('lang', function ($q) use ($name) {
			return $q->whereLike('name', $name);
		})
			->first();
		if (null === $category) {
		    if ($name == 'Готовые подарки'){
                $category = $this->create(['name' => $name, 'url' => Str::slug($name), 'product_type' => ProductTypeEnum::SET]);
            }else{
                $category = $this->create(['name' => $name, 'url' => Str::slug($name), 'product_type' => ProductTypeEnum::CANDIES]);
            }
		}

		return $category;
	}

}
