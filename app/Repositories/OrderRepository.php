<?php

namespace App\Repositories;

use App\Models\Order;

/**
 * Interface OrderRepositoryRepository.
 *
 * @package namespace App\Repositories;
 */
class OrderRepository extends AbstractRepository
{
	//
	public function model()
	{
		return Order::class;
	}

	public function create(array $attributes): Order
	{
		return parent::create($attributes);
	}
}
