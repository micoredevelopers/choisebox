<?php


namespace App\Repositories;

use App\Helpers\Media\ImageSaver;
use App\Models\Model;
use App\Models\Slider\Slider;
use App\Models\Slider\SliderItem;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

/**
 * Class MetaRepository.
 */
class SliderRepository extends AbstractRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Slider::class;
    }

    public function findForEdit(int $id)
    {
        return Slider::find($id);
    }

    public function findById(int $id): ?Slider
    {
        return (new \App\Models\Slider\Slider)->find($id);
    }


}
