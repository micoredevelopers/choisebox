<?php


namespace App\Builders\Model;


use App\Repositories\AbstractRepository;

class TemporaryBuilder
{
	/** @var AbstractRepository */
	private $repository;
	private $value = [];
	private $key;
	private $type;
	private $deletable = true;

	public function __construct(?AbstractRepository $repository = null) {
	    if (!is_null($repository)) {
	        $this->setRepository($repository);
	    }
	}

	public function setKey($key): self
	{
		$this->key = $key;
		return $this;
	}

	public function setValue($value): self
	{
		$this->value = $value;
		return $this;
	}

	//alias
	public function setData($value): self
	{
		return $this->setValue($value);
	}

	public function setRepository(AbstractRepository $repository): self
	{
		$this->repository = $repository;
		return $this;
	}

	/**
	 * @param mixed $type
	 * @return TemporaryBuilder
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}

	public function notDeletetable()
	{
		$this->deletable = false;
		return $this;
	}

	public function build()
	{
		return $this->repository->create(['type' => $this->type, 'key' => $this->key, 'value' => $this->value, 'deletable' => $this->deletable]);
	}


}