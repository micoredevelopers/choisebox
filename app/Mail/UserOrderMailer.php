<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserOrderMailer extends Mailable
{
	use Queueable, SerializesModels;

	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this
			->subject(sprintf('Оформление заказа %d', $this->order->getKey()))
			->view('mail.order.order', ['order' => $this->order])
		;
	}
}
