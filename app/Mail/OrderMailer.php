<?php

namespace App\Mail;

use App\DataContainers\Mail\MailAdminConfigDataInterface;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderMailer extends Mailable
{
	use Queueable, SerializesModels;

	/**
	 * @var Order
	 */
	private $order;

	public function __construct(Order $order, MailAdminConfigDataInterface $config)
	{
		$config->fillFromMailable($this);
		$this->order = $order;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this
			->subject(sprintf('Оформление заказа %d', $this->order->getKey()))
			->view('mail.order.order', ['order' => $this->order])
		;
	}
}
