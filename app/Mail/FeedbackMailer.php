<?php

namespace App\Mail;

use App\DataContainers\Mail\MailAdminConfigDataInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackMailer extends Mailable
{

    use Queueable, SerializesModels;

    private $data;

    public function __construct(array $data, MailAdminConfigDataInterface $config)
    {
		$config->fillFromMailable($this);
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Форма обратной связи')
            ->view('mail.feedback.feedback', $this->data);
    }
}