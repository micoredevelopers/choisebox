<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\XLSXParserController;

Route::group(
	[
		'prefix' => LaravelLocalization::setLocale(),
		'middleware' => ['localizationRedirect'],
	], function () {
	Route::post('feedback', [FeedbackController::class, 'save'])->name('feedback');

	Route::get('/category/{category:url?}', [CategoryController::class, 'show'])->name('category.show');

	Route::any('xlsx/show', [XLSXParserController::class, 'show']);
//	    Route::resource('xlsx','XLSXParserController');
	Route::post('/order/create', [OrderController::class, 'create'])->name('order.create');
	Route::get('/order/success', [OrderController::class, 'success'])->name('cart.success');

	Route::group(
		[
			'prefix' => 'cart',
		], function () {
		Route::get('', [CartController::class, 'index'])->name('cart.index');
		Route::post('/add/{id}', [CartController::class, 'add'])
			->where('id', '[0-9]+')
			->name('cart.add')
		;
        Route::post('/change/{id}', [CartController::class, 'changeAmount'])
            ->where('id', '[0-9]+')
            ->name('cart.change')
        ;
		Route::post('/plus/{id}', [CartController::class, 'changeAmountInc'])
			->where('id', '[0-9]+')
			->name('cart.plus')
		;
		Route::post('/minus/{id}', [CartController::class, 'changeAmountDec'])
			->where('id', '[0-9]+')
			->name('cart.minus')
		;
		Route::post('/cart/remove/{id}', [CartController::class, 'remove'])
			->where('id', '[0-9]+')
			->name('cart.remove')
		;
		Route::post('/cart/clear', [CartController::class, 'clear'])->name('cart.clear');
	});


	Route::get('/', [\App\Http\Controllers\HomeController::class, 'home'])->name('home');

});
